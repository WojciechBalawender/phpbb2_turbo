<?php

$phpEx = (!$phpEx) ? 'php' : $phpEx;

$md5_sum['admin/admin_account.'.$phpEx] = '3cd007c9854d7489270ab631146bc99d';
$md5_sum['admin/admin_advert.'.$phpEx] = '8673eac69eec929725f6cbb7c1af996a';
$md5_sum['admin/admin_advert_person.'.$phpEx] = '4c7e85999aaba7dda3b4c78840fbbe67';
$md5_sum['admin/admin_attach_cp.'.$phpEx] = '9a5890581e5c33fbf70711b89319c299';
$md5_sum['admin/admin_attachments.'.$phpEx] = '7cc309b1456349d1d3ad82a5d358a2e6';
$md5_sum['admin/admin_board.'.$phpEx] = '8154bd1d9ebe499b23ad7addd8871479';
$md5_sum['admin/admin_board_setup.'.$phpEx] = 'd4ec7eff5dcb84d74a2df0808c93a84b';
$md5_sum['admin/admin_category.'.$phpEx] = 'c4aed2cd74da7b20216f2b70bf30245e';
$md5_sum['admin/admin_custom.'.$phpEx] = 'da853d4c90bca0f69779f81071036a89';
$md5_sum['admin/admin_custom_fields.'.$phpEx] = '6be58a3429fe6507fa9e1d52a75795a5';
$md5_sum['admin/admin_db_utilities.'.$phpEx] = '8071d941dc294c71c86515e61d65c324';
$md5_sum['admin/admin_delete_users.'.$phpEx] = '02574afb98ad5013a86a3ed4e442d326';
$md5_sum['admin/admin_disallow.'.$phpEx] = '7aebbf2a8d9311a5302ffe0a633c7554';
$md5_sum['admin/admin_extensions.'.$phpEx] = '98291ca3f1ed9d69f8fdfcef4d04701b';
$md5_sum['admin/admin_faq_editor.'.$phpEx] = 'e37178249d776be18685abb82dee67f6';
$md5_sum['admin/admin_fchecker.'.$phpEx] = 'a9163072753184af398c91a7985f6041';
$md5_sum['admin/admin_forum_prune.'.$phpEx] = '1987397dae50d047520340e44e66f2bd';
$md5_sum['admin/admin_forumauth.'.$phpEx] = 'f6381f927377989f613fe4434ebd8f9d';
$md5_sum['admin/admin_forums.'.$phpEx] = '069a3831447a0c975095f8a56a39b8f1';
$md5_sum['admin/admin_group_rank.'.$phpEx] = 'b316cc1a17147e35869a743af758ee77';
$md5_sum['admin/admin_groups.'.$phpEx] = 'bc3f22ae99f8f14302d220349d8340d1';
$md5_sum['admin/admin_jr_admin.'.$phpEx] = '9d203e0a1175baccf1c8dd35a019eb6c';
$md5_sum['admin/admin_license.'.$phpEx] = 'eb20f5d79f615518e2b57b7c6b140998';
$md5_sum['admin/admin_logging.'.$phpEx] = '65270245d2ebe29e1b0faf82d57cec6a';
$md5_sum['admin/admin_mass_email.'.$phpEx] = '1f76f94e8dfe1f0d4213e12e05a64bb7';
$md5_sum['admin/admin_mysql.'.$phpEx] = '3d8caab48526f9842d28ba0593eb10d4';
$md5_sum['admin/admin_no_access.'.$phpEx] = '0bae9a2d4c33b813f95be46611901fb3';
$md5_sum['admin/admin_overall_forumauth.'.$phpEx] = '3a006d24300da350595cd0d828fc63b3';
$md5_sum['admin/admin_phpinfo.'.$phpEx] = 'b2ebb58ae34ba911351c1485de40f7f6';
$md5_sum['admin/admin_post_count_resync.'.$phpEx] = 'c1e412c5a3417f7568cb607556de0242';
$md5_sum['admin/admin_priv_msgs.'.$phpEx] = 'ec6fee9aa55e805fe3903239940d4008';
$md5_sum['admin/admin_prune_user_posts.'.$phpEx] = '4c0f6af0df1a1fa0870884680c56479f';
$md5_sum['admin/admin_prune_users.'.$phpEx] = '2445fcb0f29281467f460a0ef20c22e9';
$md5_sum['admin/admin_ranks.'.$phpEx] = '3dc91e1a8f83552b3ff1fbeae4cbd66c';
$md5_sum['admin/admin_rebuild_search.'.$phpEx] = '5764f5441173c561333413990801c85f';
$md5_sum['admin/admin_report.'.$phpEx] = '314e1adbe8418fc57ea04e215a9c9433';
$md5_sum['admin/admin_resync_forum_stats.'.$phpEx] = '785ca6d337246b231effdba5ecc1499a';
$md5_sum['admin/admin_settings.'.$phpEx] = '4b3e81f0bbc1a79a708fba6430f7f855';
$md5_sum['admin/admin_shoutbox.'.$phpEx] = '3a1f930f45c282916538aa7d9706fe82';
$md5_sum['admin/admin_smilies.'.$phpEx] = '5513e2bc217065212d39b1f8f711fa7f';
$md5_sum['admin/admin_sql.'.$phpEx] = 'c7fe7b5eed4d6763c0e86a001ca3bc42';
$md5_sum['admin/admin_statistics.'.$phpEx] = '845ce064d0ba1d36e53093900bd29c2b';
$md5_sum['admin/admin_styles.'.$phpEx] = '7a5f6280300693f74102d19d7f0206ad';
$md5_sum['admin/admin_ug_auth.'.$phpEx] = '9439a398d77779405a4469a2a9e9c2fb';
$md5_sum['admin/admin_uninstall.'.$phpEx] = 'd61e6b32207028eec4e1a572bb55e2da';
$md5_sum['admin/admin_user_bantron.'.$phpEx] = 'f7b0cff8415250dee82c41ad4aa64d22';
$md5_sum['admin/admin_users.'.$phpEx] = '621163c71fffccf6a25890626880b8b0';
$md5_sum['admin/admin_users_list.'.$phpEx] = '7f94a93d6edc5db2cf3026c48e0a6dc9';
$md5_sum['admin/admin_voting.'.$phpEx] = '4c1191195f5477ba5690d9a099b4d7b1';
$md5_sum['admin/admin_words.'.$phpEx] = '8fd5d53e69aa1552e6523830f7c3b32a';
$md5_sum['admin/board_setup_defaults.'.$phpEx] = '73d56b2035bc445be84d49b7c76b7f69';
$md5_sum['admin/index.'.$phpEx] = '605baa4f3cdc60dfed6ed5d684f1a3bf';
$md5_sum['admin/main_admin.'.$phpEx] = 'fd1f7c16df70de116b95bd0a68c77b26';
$md5_sum['admin/page_footer_admin.'.$phpEx] = '0cd1cf39fd2275cce5e8a91eaa7d58e2';
$md5_sum['admin/page_header_admin.'.$phpEx] = 'e62ba4014975f153e7934c7451b48f4d';
$md5_sum['admin/pagestart.'.$phpEx] = '908f0012c29fc939c2c6921f0df6355d';
$md5_sum['admin/xs_cache.'.$phpEx] = 'b42691fb4ec32a869a5c6592df8394bf';
$md5_sum['admin/xs_chmod.'.$phpEx] = '6b6f59a90dfdae336baf40528d5f78c7';
$md5_sum['admin/xs_clone.'.$phpEx] = 'ec18259dbf5e7f38240d1bf6c4887b6f';
$md5_sum['admin/xs_config.'.$phpEx] = '9ce0d76b53954f1177955dee29b40094';
$md5_sum['admin/xs_download.'.$phpEx] = 'fcbf8da5f6243d521dc58bceb2a9ded8';
$md5_sum['admin/xs_edit.'.$phpEx] = '9d8172ddc9650b4316fe53b0f5a08074';
$md5_sum['admin/xs_edit_data.'.$phpEx] = '3abe3734a77afd23ca38796a14196cf4';
$md5_sum['admin/xs_export.'.$phpEx] = '292e66a5b68335597e896a8f96361ae7';
$md5_sum['admin/xs_export_data.'.$phpEx] = '3c9f1e118e8989b4d5d69975cc8ca58a';
$md5_sum['admin/xs_frameset.'.$phpEx] = '843a4484cbbd032eb4d6b38b0742d356';
$md5_sum['admin/xs_frame_top.'.$phpEx] = 'b63b9ead79bf2a98d9f66d0839a6e728';
$md5_sum['admin/xs_import.'.$phpEx] = '60d3791077aa972638ce02f1c8bb5196';
$md5_sum['admin/xs_include.'.$phpEx] = 'bdd325cb5573355823220bfdf2c13a56';
$md5_sum['admin/xs_include_import.'.$phpEx] = 'a7f0596b4332bcdcfc02177961c9b190';
$md5_sum['admin/xs_include_import2.'.$phpEx] = 'c8b1d2d32affa64490b3cf792b2ec95f';
$md5_sum['admin/xs_index.'.$phpEx] = 'a7e874544161525dc8db9d08c73a66a8';
$md5_sum['admin/xs_install.'.$phpEx] = 'b0aa052973455204d58030e07360fbe2';
$md5_sum['admin/xs_styles.'.$phpEx] = 'a8ed51d0f25d3b7e2ff3190c22319407';
$md5_sum['admin/xs_style_config.'.$phpEx] = 'fed0cad3534e27d9e9ec591c47fc6659';
$md5_sum['admin/xs_uninstall.'.$phpEx] = '3dc283d3ddf7dda96a21fd2075d08ff8';
$md5_sum['admin/xs_update.'.$phpEx] = '1dfc312fcce15c92a682e4847d5f1268';
$md5_sum['attach_mod/includes/constants.'.$phpEx] = '8ea5f807315db3202ed57a562ad5724e';
$md5_sum['attach_mod/includes/functions_admin.'.$phpEx] = '2ee34e50597441d64e6f6d2994d619df';
$md5_sum['attach_mod/includes/functions_attach.'.$phpEx] = '12610fb43e97e22d0abd147578f8e5d3';
$md5_sum['attach_mod/includes/functions_delete.'.$phpEx] = '90c21e82201c1ad8f4b44034d403f351';
$md5_sum['attach_mod/includes/functions_filetypes.'.$phpEx] = 'd806d21648e403c799897b8ea4dcc7e5';
$md5_sum['attach_mod/includes/functions_includes.'.$phpEx] = 'd7713e2b500d876fc07ce5034d85146b';
$md5_sum['attach_mod/includes/functions_selects.'.$phpEx] = '6eccd9333869145e762851f8c6adb3dd';
$md5_sum['attach_mod/includes/functions_thumbs.'.$phpEx] = 'b50ab28194cf816125c764f083e31126';
$md5_sum['attach_mod/attachment_mod.'.$phpEx] = 'ce2d0fef4555a6dc5295c8d4eb216ae5';
$md5_sum['attach_mod/displaying.'.$phpEx] = 'c62d5f948f48d8fb8166a5b87161fbbb';
$md5_sum['attach_mod/pm_attachments.'.$phpEx] = '80ed72aa17f3c6d257451fe361b31100';
$md5_sum['attach_mod/posting_attachments.'.$phpEx] = '8f421a81c0b9f4d7324c936d137dbd19';
$md5_sum['db/schemas/mysql_schema.sql'] = '2ac6703739ed558e52359a35581216df';
$md5_sum['db/schemas/mysql_basic.sql'] = 'c3d504f005c3df1d8da7f745f6df9a33';
$md5_sum['db/mysql.'.$phpEx] = '581e31a282d7408ef090b161fc661b11';
$md5_sum['db/mysql4.'.$phpEx] = 'b9384d328a7bb1c8670ffa9ab7ece3eb';
$md5_sum['db/mysqli.'.$phpEx] = '281ba78820261463fdd249b7b16e8c3d';
$md5_sum['dbloader/dbdata.inc.'.$phpEx] = 'bce72be22147d6d3dbab1361b2003bf3';
$md5_sum['dbloader/dbloader.'.$phpEx] = 'a175a5138a57fa547f8847516fed90c8';
$md5_sum['dbloader/FileReader.class.'.$phpEx] = 'c8304d25f27674df0a5fd45e7ac9b1c9';
$md5_sum['dbloader/functions.'.$phpEx] = '5ec4ad78187aa53b7d72f3e52ff474dd';
$md5_sum['dbloader/read_dump.lib.'.$phpEx] = 'b74a96d7b4837692e62ed88b437d68e0';
$md5_sum['dbloader/SQLReader.class.'.$phpEx] = '43b41d3adeb1d97754870ec4fd9defd1';
$md5_sum['dbloader/Timer.class.'.$phpEx] = '41f997b33484585ca1c106639bd49287';
$md5_sum['includes/auth.'.$phpEx] = 'fb205a6cc57fab7a718c65f86cdd1b31';
$md5_sum['includes/bbcode.'.$phpEx] = 'a34b9c35ed8ef48227b9f7891a7b608b';
$md5_sum['includes/confirm_register.'.$phpEx] = 'd944d55dd008f770f5b3046e7ffc3448';
$md5_sum['includes/constants.'.$phpEx] = '097af4b28a02e09bc0076c5214a57f45';
$md5_sum['includes/db.'.$phpEx] = '1ed4099a455c7e656f4e4519f213f790';
$md5_sum['includes/emailer.'.$phpEx] = 'ed10f8ee15783944cd4ac679163066da';
$md5_sum['includes/functions.'.$phpEx] = 'e2043bb30821051cda5b1ec4868cf5a2';
$md5_sum['includes/functions_add.'.$phpEx] = 'f54c9a18ec670583477e0780659f4ba4';
$md5_sum['includes/functions_admin.'.$phpEx] = '2dd2af4c45828937dcd32df4f65626c6';
$md5_sum['includes/functions_hierarchy.'.$phpEx] = 'ec03da28b1234af86f79f9cd38886d9b';
$md5_sum['includes/functions_log.'.$phpEx] = 'b90dba107e4148914d56aab4677a9c52';
$md5_sum['includes/functions_module.'.$phpEx] = '3c5fb0307520fafedf34da88a03e9e45';
$md5_sum['includes/functions_post.'.$phpEx] = '53994ec0565576531fcfc0a5230ee976';
$md5_sum['includes/functions_remove.'.$phpEx] = '61ee857d0ce11d5bc7cd78df41cf3b13';
$md5_sum['includes/functions_search.'.$phpEx] = 'a616bc07c57bfb78b0f3bc7c3158d6a9';
$md5_sum['includes/functions_selects.'.$phpEx] = '9df45737cfaf3b9b09b6e66410d3765a';
$md5_sum['includes/functions_stats.'.$phpEx] = '6d0dadd574e07609bb7ebdf0a1bc0a41';
$md5_sum['includes/functions_validate.'.$phpEx] = 'f0bc850868921e7c213aa4321e75c2af';
$md5_sum['includes/page_header.'.$phpEx] = '2f418d0454e289686677998d945683a1';
$md5_sum['includes/page_tail.'.$phpEx] = 'bb0dbcb2d405b4d430c555eb03fc654b';
$md5_sum['includes/read_history.'.$phpEx] = '2e8ffa6d4d5367217f22992e8cdd9a63';
$md5_sum['includes/reportpost.'.$phpEx] = '8d87237c70d29121f9f3e3f9fc6b2178';
$md5_sum['includes/sessions.'.$phpEx] = 'f8347d8f9444c2bb7a75e86d15e8f0a8';
$md5_sum['includes/smtp.'.$phpEx] = 'b6432351bea044b7ada52096d1742af8';
$md5_sum['includes/sql_parse.'.$phpEx] = 'd86014563838a8542b9feb6713057485';
$md5_sum['includes/template.'.$phpEx] = '844e3af7eec262882a5b607b68f8243e';
$md5_sum['includes/template_old.'.$phpEx] = '5204deb71e37f914dcacb06116981665';
$md5_sum['includes/topic_review.'.$phpEx] = '92192a2e457b30c7d4e77735520da570';
$md5_sum['includes/usercp_activate.'.$phpEx] = 'bcad8f7e2bfae79fea1fd8906a10a416';
$md5_sum['includes/usercp_avatar.'.$phpEx] = 'f5ccf3b051f616fcee490a059d7c7793';
$md5_sum['includes/usercp_email.'.$phpEx] = 'bde1dc721cf8e0349fb7023f6833eb25';
$md5_sum['includes/usercp_register.'.$phpEx] = 'c7e8ec804aeb778f442aeb369eec799f';
$md5_sum['includes/usercp_sendpasswd.'.$phpEx] = 'eb7c25de9b203cd949198d7c1f1ec5af';
$md5_sum['includes/usercp_signature.'.$phpEx] = 'b6e5bba88c8071621b718df8f890ca28';
$md5_sum['includes/usercp_viewprofile.'.$phpEx] = '4f032d44fc3059986bcf7695c8b196f3';
$md5_sum['language/lang_english/lang_admin.'.$phpEx] = '87662805a39a32bb6f4f287fe680d55b';
$md5_sum['language/lang_english/lang_admin_advert.'.$phpEx] = '85ca953f6d38b4e74b67bd4c155cbaba';
$md5_sum['language/lang_english/lang_admin_attach.'.$phpEx] = '6d82a9544b2ffefa587e7713e041087d';
$md5_sum['language/lang_english/lang_admin_board.'.$phpEx] = '913175ecae8d7d7ad0f071329ddd0001';
$md5_sum['language/lang_english/lang_admin_priv_msgs.'.$phpEx] = '322555fad85260769340e56f3c891e32';
$md5_sum['language/lang_english/lang_admin_voting.'.$phpEx] = 'd891f75ae65bc0481cea116e512b7a24';
$md5_sum['language/lang_english/lang_bbcode.'.$phpEx] = 'cb09a64b42e57f6f6fe6aa888e700379';
$md5_sum['language/lang_english/lang_check_files.'.$phpEx] = 'c6f6a7f1bc159fbd36c00e6b9ff9869f';
$md5_sum['language/lang_english/lang_custom_fields.'.$phpEx] = 'fd0c70ee90f009ae047d1ff051cfd4e3';
$md5_sum['language/lang_english/lang_customize.'.$phpEx] = '563c6d57960a777b577bb937b6b1a52f';
$md5_sum['language/lang_english/lang_faq_editor.'.$phpEx] = '7e8f24c43673554af9098e9edaffb8eb';
$md5_sum['language/lang_english/lang_install.'.$phpEx] = '89aea2aa6c4484ac23bbff6be5c1fdfb';
$md5_sum['language/lang_english/lang_jr_admin.'.$phpEx] = '07d8bce21ad5854415118df29d754dc8';
$md5_sum['language/lang_english/lang_main.'.$phpEx] = 'ad3401a2ed3cdb1d4414b82ca7a71c56';
$md5_sum['language/lang_english/lang_main_attach.'.$phpEx] = 'aadf989db141aebf991f915a9d463d0f';
$md5_sum['language/lang_english/lang_mass_email.'.$phpEx] = '74dee54fe27b80daf4bb24deb115c9f3';
$md5_sum['language/lang_english/lang_modcp.'.$phpEx] = 'f91c49d027c2a8c58b25e7158f1401f6';
$md5_sum['language/lang_english/lang_pcount_resync.'.$phpEx] = '110e58cc8b2ec2134941536d1e302c4b';
$md5_sum['language/lang_english/lang_post_history.'.$phpEx] = 'd6cca320fc6c204cb9875e448ab2f1c7';
$md5_sum['language/lang_english/lang_profile.'.$phpEx] = 'f211a4ea5cf8c7d49ee986683dcce69c';
$md5_sum['language/lang_english/lang_prune_users.'.$phpEx] = '029c50d75ce6b0cbc7f3ecf08545d309';
$md5_sum['language/lang_english/lang_seeker.'.$phpEx] = '1b08e0c4de631d35537083a9bc8d6aa2';
$md5_sum['language/lang_english/lang_statistics.'.$phpEx] = '0814946101e7c3f21b822c854d96d867';
$md5_sum['language/lang_english/lang_warnings.'.$phpEx] = '326a2228a33c304381031c5a92525066';
$md5_sum['language/lang_english/lang_xs.'.$phpEx] = '75d0308a28c5ffbfd42d5583b677af32';
$md5_sum['language/lang_polish/lang_admin.'.$phpEx] = '0ebbde28d8dddee0379001c8d7ab2c3a';
$md5_sum['language/lang_polish/lang_admin_advert.'.$phpEx] = '2da020020a1b9670fa072367f6172825';
$md5_sum['language/lang_polish/lang_admin_attach.'.$phpEx] = 'a89730cda7e72c7ad1a4a7c6d28544b2';
$md5_sum['language/lang_polish/lang_admin_board.'.$phpEx] = '71517f077fbd0804d0f84f9f24375796';
$md5_sum['language/lang_polish/lang_admin_priv_msgs.'.$phpEx] = 'fbb7245d837d5b558f3adf26f4e579e6';
$md5_sum['language/lang_polish/lang_admin_voting.'.$phpEx] = '9754a4f5b8276c15c8c0244418cb6044';
$md5_sum['language/lang_polish/lang_bbcode.'.$phpEx] = 'ea15567c97d109722625ef6b578eec78';
$md5_sum['language/lang_polish/lang_check_files.'.$phpEx] = 'b8dce24dabc94a20f7e937248b97430f';
$md5_sum['language/lang_polish/lang_custom_fields.'.$phpEx] = 'a316edf0e01c756615e46cefaf30e27f';
$md5_sum['language/lang_polish/lang_customize.'.$phpEx] = '10e4c8d765ef38b91b727dc023321a80';
$md5_sum['language/lang_polish/lang_faq_editor.'.$phpEx] = 'bdfde8e0d1cb4c2ad81b9c549a2c2517';
$md5_sum['language/lang_polish/lang_install.'.$phpEx] = '1cd8bc4cbef973ccdb62d59f55355a66';
$md5_sum['language/lang_polish/lang_jr_admin.'.$phpEx] = '59d1837ac9dbf6437137686709d421fb';
$md5_sum['language/lang_polish/lang_main.'.$phpEx] = '6b208d2fd56a1107670f2a08cca111c8';
$md5_sum['language/lang_polish/lang_main_attach.'.$phpEx] = '75a11aa2281dc6f1e6f2328e5d977844';
$md5_sum['language/lang_polish/lang_mass_email.'.$phpEx] = 'cf63951fcfa4b2a1a62460c9ca422d40';
$md5_sum['language/lang_polish/lang_modcp.'.$phpEx] = 'b4193749d00a82f1809536d421eb2632';
$md5_sum['language/lang_polish/lang_pcount_resync.'.$phpEx] = '850b4fa81a94402c5106b5dce7c179a0';
$md5_sum['language/lang_polish/lang_post_history.'.$phpEx] = 'dc3fa2025b69fcfc6c6857fc20c57e9f';
$md5_sum['language/lang_polish/lang_profile.'.$phpEx] = '63ca2332af94511df238f692a3f0a4c9';
$md5_sum['language/lang_polish/lang_prune_users.'.$phpEx] = '54dfcaa35134d0ceea4f47f1a868ec7a';
$md5_sum['language/lang_polish/lang_seeker.'.$phpEx] = 'b5e2d9340abce5718281e50b91349321';
$md5_sum['language/lang_polish/lang_statistics.'.$phpEx] = '17aa79c9c14d44edd4f6e57c8a9cb7b6';
$md5_sum['language/lang_polish/lang_warnings.'.$phpEx] = '308f30b6b29f9688b472693dea1ddb11';
$md5_sum['language/lang_polish/lang_xs.'.$phpEx] = '87399b485c845c78cb9489265ecd2688';
$md5_sum['ad.'.$phpEx] = '0256c1a9e03af64e970a8411d6ba5e89';
$md5_sum['attach_rules.'.$phpEx] = 'df866a46c8b6d740473853e3f9404682';
$md5_sum['common.'.$phpEx] = 'd5522b7bf2f306ac2d582c0f3bd7fd5d';
$md5_sum['customize.'.$phpEx] = 'bb6e2f6a3175b4069fc5f86116724ccb';
$md5_sum['download.'.$phpEx] = 'f0be6bbec4eb8a438a7f960ae08269bd';
$md5_sum['extension.inc'] = '15f5413201022e7f09fc012e337aeb8b';
$md5_sum['faq.'.$phpEx] = '86497db9edf5939f2b5d06cf6547acd9';
$md5_sum['fetchposts.'.$phpEx] = 'fe883962c193545e327dadea2c1ee8eb';
$md5_sum['groupcp.'.$phpEx] = '6ccd248ac6b692a83bf792330ec667b8';
$md5_sum['groupcp_mail.'.$phpEx] = '30a02a1ee4427cea1209b225e1e8eb7c';
$md5_sum['ignore.'.$phpEx] = 'e808bd947cec021c7f55ea31775a581b';
$md5_sum['ignore_topics.'.$phpEx] = 'e223e35667854288ec69f57f7fada29e';
$md5_sum['index.'.$phpEx] = '348e423a5c51c2e578bd8f1e7b7bafe3';
$md5_sum['login.'.$phpEx] = 'db243a2cfb87c721333c8e7ae34a4a06';
$md5_sum['memberlist.'.$phpEx] = '0752039e1130324fb876eb3e9d7bcc84';
$md5_sum['modcp.'.$phpEx] = '8ab41cf62e31ad41648620660ed5eb5d';
$md5_sum['post_history.'.$phpEx] = '57c10912ece4896d48b76746e100845b';
$md5_sum['posting.'.$phpEx] = '49ee53222ce5e4f8a10f8a987fbbbad5';
$md5_sum['printview.'.$phpEx] = '4395c458f13b71a0aa23ec5204e65f8e';
$md5_sum['privmsg.'.$phpEx] = 'cc9849aa9c5393b1c1bf8933ea08dd40';
$md5_sum['profile.'.$phpEx] = 'f28a8b4b4bd26e5979b3e94307e57e1c';
$md5_sum['profilephoto_mod.'.$phpEx] = '18e8e38360f23767702e241fce6b862e';
$md5_sum['quick_reply.'.$phpEx] = '2b49002e894a4d8073e87d93d2ff4c9e';
$md5_sum['report.'.$phpEx] = 'd2d0f8ea5bcff847793051c79b223e31';
$md5_sum['resync_forum_stats.'.$phpEx] = 'f002e1ed1ed2ae767a124777ca8d07dc';
$md5_sum['search.'.$phpEx] = '103b0791589341d1b513aaf8eb0b16fb';
$md5_sum['seeker.'.$phpEx] = '349a0532735f53b0e0b3541cf4a65765';
$md5_sum['shoutbox.'.$phpEx] = '9262a96edb1ec80582e7a572e4cb9cd8';
$md5_sum['shoutbox_view.'.$phpEx] = '5da4cbd0489b20b1a33be983b8690907';
$md5_sum['staff.'.$phpEx] = '428f29eda2f7634ec217bd865facbc5d';
$md5_sum['tellafriend.'.$phpEx] = 'faa692a38e29baf1a96191e8fd9c2ebc';
$md5_sum['topic_spy.'.$phpEx] = '1f7ce217cd6c8512229e6b5f12873e6d';
$md5_sum['topic_view_users.'.$phpEx] = '3a728e9da9141bfcb485327eb565e9f2';
$md5_sum['uacp.'.$phpEx] = '37117f565e3287cfc120638781cf3076';
$md5_sum['viewforum.'.$phpEx] = '44509383513eb01db0357548dd061a53';
$md5_sum['viewonline.'.$phpEx] = 'd2ecfa708c7b6f6a3efa85d243eeb57c';
$md5_sum['viewtopic.'.$phpEx] = '09476d42624756e31cdd34c078ba7b48';
$md5_sum['warnings.'.$phpEx] = '2ea449285e1462ba5b7d8d6ec211434d';
$md5_sum['m.'.$phpEx] = 'f1e86bb48994bc2c9fc83d2d026b34c8';
$md5_sum['restore.'.$phpEx] = '1637b2a2e6b6276aae4e0a8b3ed6145b';

$file_list = array_keys($md5_sum);
$sizes['admin/admin_account.'.$phpEx] = '6416';
$sizes['admin/admin_advert.'.$phpEx] = '11099';
$sizes['admin/admin_advert_person.'.$phpEx] = '4661';
$sizes['admin/admin_attach_cp.'.$phpEx] = '22024';
$sizes['admin/admin_attachments.'.$phpEx] = '38729';
$sizes['admin/admin_board.'.$phpEx] = '51821';
$sizes['admin/admin_board_setup.'.$phpEx] = '5334';
$sizes['admin/admin_category.'.$phpEx] = '9843';
$sizes['admin/admin_custom.'.$phpEx] = '7138';
$sizes['admin/admin_custom_fields.'.$phpEx] = '14559';
$sizes['admin/admin_db_utilities.'.$phpEx] = '10478';
$sizes['admin/admin_delete_users.'.$phpEx] = '3331';
$sizes['admin/admin_disallow.'.$phpEx] = '3991';
$sizes['admin/admin_extensions.'.$phpEx] = '25829';
$sizes['admin/admin_faq_editor.'.$phpEx] = '15837';
$sizes['admin/admin_fchecker.'.$phpEx] = '4346';
$sizes['admin/admin_forum_prune.'.$phpEx] = '4340';
$sizes['admin/admin_forumauth.'.$phpEx] = '8134';
$sizes['admin/admin_forums.'.$phpEx] = '51263';
$sizes['admin/admin_group_rank.'.$phpEx] = '4999';
$sizes['admin/admin_groups.'.$phpEx] = '17227';
$sizes['admin/admin_jr_admin.'.$phpEx] = '3758';
$sizes['admin/admin_license.'.$phpEx] = '7015';
$sizes['admin/admin_logging.'.$phpEx] = '8211';
$sizes['admin/admin_mass_email.'.$phpEx] = '11196';
$sizes['admin/admin_mysql.'.$phpEx] = '3162';
$sizes['admin/admin_no_access.'.$phpEx] = '1265';
$sizes['admin/admin_overall_forumauth.'.$phpEx] = '6898';
$sizes['admin/admin_phpinfo.'.$phpEx] = '4384';
$sizes['admin/admin_post_count_resync.'.$phpEx] = '12557';
$sizes['admin/admin_priv_msgs.'.$phpEx] = '8114';
$sizes['admin/admin_prune_user_posts.'.$phpEx] = '3692';
$sizes['admin/admin_prune_users.'.$phpEx] = '7243';
$sizes['admin/admin_ranks.'.$phpEx] = '10849';
$sizes['admin/admin_rebuild_search.'.$phpEx] = '7273';
$sizes['admin/admin_report.'.$phpEx] = '9927';
$sizes['admin/admin_resync_forum_stats.'.$phpEx] = '12836';
$sizes['admin/admin_settings.'.$phpEx] = '7081';
$sizes['admin/admin_shoutbox.'.$phpEx] = '8552';
$sizes['admin/admin_smilies.'.$phpEx] = '20580';
$sizes['admin/admin_sql.'.$phpEx] = '1912';
$sizes['admin/admin_statistics.'.$phpEx] = '22162';
$sizes['admin/admin_styles.'.$phpEx] = '25538';
$sizes['admin/admin_ug_auth.'.$phpEx] = '31915';
$sizes['admin/admin_uninstall.'.$phpEx] = '12436';
$sizes['admin/admin_user_bantron.'.$phpEx] = '21480';
$sizes['admin/admin_users.'.$phpEx] = '60635';
$sizes['admin/admin_users_list.'.$phpEx] = '12100';
$sizes['admin/admin_voting.'.$phpEx] = '8062';
$sizes['admin/admin_words.'.$phpEx] = '5735';
$sizes['admin/board_setup_defaults.'.$phpEx] = '23865';
$sizes['admin/index.'.$phpEx] = '23397';
$sizes['admin/main_admin.'.$phpEx] = '1086';
$sizes['admin/page_footer_admin.'.$phpEx] = '1287';
$sizes['admin/page_header_admin.'.$phpEx] = '4877';
$sizes['admin/pagestart.'.$phpEx] = '3073';
$sizes['admin/xs_cache.'.$phpEx] = '5766';
$sizes['admin/xs_chmod.'.$phpEx] = '2312';
$sizes['admin/xs_clone.'.$phpEx] = '9629';
$sizes['admin/xs_config.'.$phpEx] = '6832';
$sizes['admin/xs_download.'.$phpEx] = '4924';
$sizes['admin/xs_edit.'.$phpEx] = '14055';
$sizes['admin/xs_edit_data.'.$phpEx] = '11998';
$sizes['admin/xs_export.'.$phpEx] = '8944';
$sizes['admin/xs_export_data.'.$phpEx] = '6420';
$sizes['admin/xs_frameset.'.$phpEx] = '2765';
$sizes['admin/xs_frame_top.'.$phpEx] = '1548';
$sizes['admin/xs_import.'.$phpEx] = '11408';
$sizes['admin/xs_include.'.$phpEx] = '27505';
$sizes['admin/xs_include_import.'.$phpEx] = '2107';
$sizes['admin/xs_include_import2.'.$phpEx] = '11522';
$sizes['admin/xs_index.'.$phpEx] = '2052';
$sizes['admin/xs_install.'.$phpEx] = '4466';
$sizes['admin/xs_styles.'.$phpEx] = '7278';
$sizes['admin/xs_style_config.'.$phpEx] = '4583';
$sizes['admin/xs_uninstall.'.$phpEx] = '5972';
$sizes['admin/xs_update.'.$phpEx] = '7221';
$sizes['attach_mod/includes/constants.'.$phpEx] = '1968';
$sizes['attach_mod/includes/functions_admin.'.$phpEx] = '10734';
$sizes['attach_mod/includes/functions_attach.'.$phpEx] = '18374';
$sizes['attach_mod/includes/functions_delete.'.$phpEx] = '6793';
$sizes['attach_mod/includes/functions_filetypes.'.$phpEx] = '5099';
$sizes['attach_mod/includes/functions_includes.'.$phpEx] = '13283';
$sizes['attach_mod/includes/functions_selects.'.$phpEx] = '5258';
$sizes['attach_mod/includes/functions_thumbs.'.$phpEx] = '4270';
$sizes['attach_mod/attachment_mod.'.$phpEx] = '1728';
$sizes['attach_mod/displaying.'.$phpEx] = '22608';
$sizes['attach_mod/pm_attachments.'.$phpEx] = '6205';
$sizes['attach_mod/posting_attachments.'.$phpEx] = '39562';
$sizes['db/schemas/mysql_schema.sql'] = '35408';
$sizes['db/schemas/mysql_basic.sql'] = '70997';
$sizes['db/mysql.'.$phpEx] = '5466';
$sizes['db/mysql4.'.$phpEx] = '5814';
$sizes['db/mysqli.'.$phpEx] = '4701';
$sizes['dbloader/dbdata.inc.'.$phpEx] = '3139';
$sizes['dbloader/dbloader.'.$phpEx] = '29177';
$sizes['dbloader/FileReader.class.'.$phpEx] = '3395';
$sizes['dbloader/functions.'.$phpEx] = '9887';
$sizes['dbloader/read_dump.lib.'.$phpEx] = '2921';
$sizes['dbloader/SQLReader.class.'.$phpEx] = '2415';
$sizes['dbloader/Timer.class.'.$phpEx] = '732';
$sizes['includes/auth.'.$phpEx] = '8138';
$sizes['includes/bbcode.'.$phpEx] = '28159';
$sizes['includes/confirm_register.'.$phpEx] = '1804';
$sizes['includes/constants.'.$phpEx] = '7197';
$sizes['includes/db.'.$phpEx] = '1221';
$sizes['includes/emailer.'.$phpEx] = '9289';
$sizes['includes/functions.'.$phpEx] = '68269';
$sizes['includes/functions_add.'.$phpEx] = '29214';
$sizes['includes/functions_admin.'.$phpEx] = '13162';
$sizes['includes/functions_hierarchy.'.$phpEx] = '15493';
$sizes['includes/functions_log.'.$phpEx] = '1783';
$sizes['includes/functions_module.'.$phpEx] = '9202';
$sizes['includes/functions_post.'.$phpEx] = '31358';
$sizes['includes/functions_remove.'.$phpEx] = '17635';
$sizes['includes/functions_search.'.$phpEx] = '11331';
$sizes['includes/functions_selects.'.$phpEx] = '6421';
$sizes['includes/functions_stats.'.$phpEx] = '7449';
$sizes['includes/functions_validate.'.$phpEx] = '6074';
$sizes['includes/page_header.'.$phpEx] = '25311';
$sizes['includes/page_tail.'.$phpEx] = '2566';
$sizes['includes/read_history.'.$phpEx] = '6862';
$sizes['includes/reportpost.'.$phpEx] = '10597';
$sizes['includes/sessions.'.$phpEx] = '26647';
$sizes['includes/smtp.'.$phpEx] = '5003';
$sizes['includes/sql_parse.'.$phpEx] = '4145';
$sizes['includes/template.'.$phpEx] = '48840';
$sizes['includes/template_old.'.$phpEx] = '6683';
$sizes['includes/topic_review.'.$phpEx] = '8384';
$sizes['includes/usercp_activate.'.$phpEx] = '4135';
$sizes['includes/usercp_avatar.'.$phpEx] = '13804';
$sizes['includes/usercp_email.'.$phpEx] = '5636';
$sizes['includes/usercp_register.'.$phpEx] = '63489';
$sizes['includes/usercp_sendpasswd.'.$phpEx] = '3913';
$sizes['includes/usercp_signature.'.$phpEx] = '5598';
$sizes['includes/usercp_viewprofile.'.$phpEx] = '31306';
$sizes['language/lang_english/lang_admin.'.$phpEx] = '48811';
$sizes['language/lang_english/lang_admin_advert.'.$phpEx] = '3215';
$sizes['language/lang_english/lang_admin_attach.'.$phpEx] = '16185';
$sizes['language/lang_english/lang_admin_board.'.$phpEx] = '26972';
$sizes['language/lang_english/lang_admin_priv_msgs.'.$phpEx] = '708';
$sizes['language/lang_english/lang_admin_voting.'.$phpEx] = '1238';
$sizes['language/lang_english/lang_bbcode.'.$phpEx] = '10120';
$sizes['language/lang_english/lang_check_files.'.$phpEx] = '5162';
$sizes['language/lang_english/lang_custom_fields.'.$phpEx] = '3643';
$sizes['language/lang_english/lang_customize.'.$phpEx] = '1966';
$sizes['language/lang_english/lang_faq_editor.'.$phpEx] = '1545';
$sizes['language/lang_english/lang_install.'.$phpEx] = '5111';
$sizes['language/lang_english/lang_jr_admin.'.$phpEx] = '336';
$sizes['language/lang_english/lang_main.'.$phpEx] = '45641';
$sizes['language/lang_english/lang_main_attach.'.$phpEx] = '7731';
$sizes['language/lang_english/lang_mass_email.'.$phpEx] = '1787';
$sizes['language/lang_english/lang_modcp.'.$phpEx] = '3729';
$sizes['language/lang_english/lang_pcount_resync.'.$phpEx] = '2486';
$sizes['language/lang_english/lang_post_history.'.$phpEx] = '1043';
$sizes['language/lang_english/lang_profile.'.$phpEx] = '13301';
$sizes['language/lang_english/lang_prune_users.'.$phpEx] = '2893';
$sizes['language/lang_english/lang_seeker.'.$phpEx] = '534';
$sizes['language/lang_english/lang_statistics.'.$phpEx] = '3931';
$sizes['language/lang_english/lang_warnings.'.$phpEx] = '4008';
$sizes['language/lang_english/lang_xs.'.$phpEx] = '32551';
$sizes['language/lang_polish/lang_admin.'.$phpEx] = '54085';
$sizes['language/lang_polish/lang_admin_advert.'.$phpEx] = '3508';
$sizes['language/lang_polish/lang_admin_attach.'.$phpEx] = '14486';
$sizes['language/lang_polish/lang_admin_board.'.$phpEx] = '32336';
$sizes['language/lang_polish/lang_admin_priv_msgs.'.$phpEx] = '716';
$sizes['language/lang_polish/lang_admin_voting.'.$phpEx] = '1254';
$sizes['language/lang_polish/lang_bbcode.'.$phpEx] = '9866';
$sizes['language/lang_polish/lang_check_files.'.$phpEx] = '5896';
$sizes['language/lang_polish/lang_custom_fields.'.$phpEx] = '4464';
$sizes['language/lang_polish/lang_customize.'.$phpEx] = '2146';
$sizes['language/lang_polish/lang_faq_editor.'.$phpEx] = '1596';
$sizes['language/lang_polish/lang_install.'.$phpEx] = '6504';
$sizes['language/lang_polish/lang_jr_admin.'.$phpEx] = '446';
$sizes['language/lang_polish/lang_main.'.$phpEx] = '48896';
$sizes['language/lang_polish/lang_main_attach.'.$phpEx] = '7466';
$sizes['language/lang_polish/lang_mass_email.'.$phpEx] = '2091';
$sizes['language/lang_polish/lang_modcp.'.$phpEx] = '4155';
$sizes['language/lang_polish/lang_pcount_resync.'.$phpEx] = '2133';
$sizes['language/lang_polish/lang_post_history.'.$phpEx] = '1123';
$sizes['language/lang_polish/lang_profile.'.$phpEx] = '13884';
$sizes['language/lang_polish/lang_prune_users.'.$phpEx] = '3098';
$sizes['language/lang_polish/lang_seeker.'.$phpEx] = '609';
$sizes['language/lang_polish/lang_statistics.'.$phpEx] = '3600';
$sizes['language/lang_polish/lang_warnings.'.$phpEx] = '4834';
$sizes['language/lang_polish/lang_xs.'.$phpEx] = '32363';
$sizes['ad.'.$phpEx] = '3063';
$sizes['attach_rules.'.$phpEx] = '3442';
$sizes['common.'.$phpEx] = '10847';
$sizes['customize.'.$phpEx] = '8260';
$sizes['download.'.$phpEx] = '11528';
$sizes['extension.inc'] = '443';
$sizes['faq.'.$phpEx] = '3887';
$sizes['fetchposts.'.$phpEx] = '4883';
$sizes['groupcp.'.$phpEx] = '40035';
$sizes['groupcp_mail.'.$phpEx] = '10556';
$sizes['ignore.'.$phpEx] = '7890';
$sizes['ignore_topics.'.$phpEx] = '8338';
$sizes['index.'.$phpEx] = '26509';
$sizes['login.'.$phpEx] = '16020';
$sizes['memberlist.'.$phpEx] = '16023';
$sizes['modcp.'.$phpEx] = '68594';
$sizes['post_history.'.$phpEx] = '6249';
$sizes['posting.'.$phpEx] = '75656';
$sizes['printview.'.$phpEx] = '8601';
$sizes['privmsg.'.$phpEx] = '74393';
$sizes['profile.'.$phpEx] = '3371';
$sizes['profilephoto_mod.'.$phpEx] = '13788';
$sizes['quick_reply.'.$phpEx] = '4374';
$sizes['report.'.$phpEx] = '11290';
$sizes['resync_forum_stats.'.$phpEx] = '11864';
$sizes['search.'.$phpEx] = '43754';
$sizes['seeker.'.$phpEx] = '10411';
$sizes['shoutbox.'.$phpEx] = '1247';
$sizes['shoutbox_view.'.$phpEx] = '11887';
$sizes['staff.'.$phpEx] = '8720';
$sizes['tellafriend.'.$phpEx] = '4677';
$sizes['topic_spy.'.$phpEx] = '4863';
$sizes['topic_view_users.'.$phpEx] = '11991';
$sizes['uacp.'.$phpEx] = '12383';
$sizes['viewforum.'.$phpEx] = '32846';
$sizes['viewonline.'.$phpEx] = '7593';
$sizes['viewtopic.'.$phpEx] = '95322';
$sizes['warnings.'.$phpEx] = '23486';

$tables_structure = array(
'advertisement' => array(
	'id',
	'html',
	'email',
	'clicks',
	'position',
	'porder',
	'added',
	'expire',
	'last_update',
	'notify',
	'type'),
'adv_person' => array(
	'user_id',
	'person_id',
	'person_ip'),

'anti_robotic_reg' => array(
	'session_id',
	'reg_key',
	'timestamp'),
'attachments' => array(
	'attach_id',
	'post_id',
	'privmsgs_id',
	'user_id_1',
	'user_id_2'),
'attachments_config' => array(
	'config_name',
	'config_value'),
'attachments_desc' => array(
	'attach_id',
	'physical_filename',
	'real_filename',
	'download_count',
	'comment',
	'extension',
	'mimetype',
	'filesize',
	'filetime',
	'thumbnail'),
'attach_quota' => array(
	'user_id',
	'group_id',
	'quota_type',
	'quota_limit_id'),
'auth_access' => array(
	'group_id',
	'forum_id',
	'auth_view',
	'auth_read',
	'auth_post',
	'auth_reply',
	'auth_edit',
	'auth_delete',
	'auth_sticky',
	'auth_announce',
	'auth_globalannounce',
	'auth_vote',
	'auth_pollcreate',
	'auth_attachments',
	'auth_mod',
	'auth_download'),
'banlist' => array(
	'ban_id',
	'ban_userid',
	'ban_ip',
	'ban_email',
	'ban_time',
	'ban_expire_time',
	'ban_by_userid',
	'ban_priv_reason',
	'ban_pub_reason_mode',
	'ban_pub_reason',
	'ban_host'),
'birthday' => array(
	'user_id',
	'send_user_id',
	'send_year'),
'categories' => array(
	'cat_id',
	'cat_title',
	'cat_order',
	'cat_main_type',
	'cat_main',
	'cat_desc'),
'config' => array(
	'config_name',
	'config_value'),
'disallow' => array(
	'disallow_id',
	'disallow_username'),
'extensions' => array(
	'ext_id',
	'group_id',
	'extension',
	'comment'),
'extension_groups' => array(
	'group_id',
	'group_name',
	'cat_id',
	'allow_group',
	'download_mode',
	'upload_icon',
	'max_filesize',
	'forum_permissions'),
'forbidden_extensions' => array(
	'ext_id',
	'extension'),
'forum_prune' => array(
	'prune_id',
	'forum_id',
	'prune_days',
	'prune_freq'),
'forums' => array(
	'forum_id',
	'cat_id',
	'forum_name',
	'forum_desc',
	'forum_status',
	'forum_order',
	'forum_posts',
	'forum_topics',
	'forum_last_post_id',
	'prune_next',
	'prune_enable',
	'auth_view',
	'auth_read',
	'auth_post',
	'auth_reply',
	'auth_edit',
	'auth_delete',
	'auth_sticky',
	'auth_announce',
	'auth_globalannounce',
	'auth_vote',
	'auth_pollcreate',
	'auth_attachments',
	'auth_download',
	'password',
	'forum_sort',
	'forum_color',
	'forum_link',
	'forum_link_internal',
	'forum_link_hit_count',
	'forum_link_hit',
	'main_type',
	'forum_moderate',
	'no_count',
	'forum_trash',
	'forum_separate',
	'forum_show_ga',
	'forum_tree_grade',
	'forum_tree_req',
	'forum_no_split',
	'forum_no_helped',
	'topic_tags',
	'locked_bottom'),
'groups' => array(
	'group_id',
	'group_type',
	'group_name',
	'group_description',
	'group_moderator',
	'group_single_user',
	'group_order',
	'group_count',
	'group_count_enable',
	'group_mail_enable',
	'group_no_unsub',
	'group_color',
	'group_prefix',
	'group_style'),
'ignores' => array(
	'user_id',
	'user_ignore'),
'jr_admin_users' => array(
	'user_id',
	'user_jr_admin'),
'logs' => array(
	'id_log',
	'mode',
	'topic_id',
	'user_id',
	'username',
	'user_ip',
	'time'),
'mass_email' => array(
	'mass_email_user_id',
	'mass_email_text',
	'mass_email_subject',
	'mass_email_bcc',
	'mass_email_html',
	'mass_email_to'),
'pa_cat' => array(
	'cat_id',
	'cat_name',
	'cat_desc',
	'cat_files',
	'cat_1xid',
	'cat_parent',
	'cat_order'),
'pa_comments' => array(
	'comments_id',
	'file_id',
	'comments_text',
	'comments_title',
	'comments_time',
	'comment_bbcode_uid',
	'poster_id'),
'pa_custom' => array(
	'custom_id',
	'custom_name',
	'custom_description'),
'pa_customdata' => array(
	'customdata_file',
	'customdata_custom',
	'data'),
'pa_files' => array(
	'file_id',
	'file_name',
	'file_desc',
	'file_creator',
	'file_version',
	'file_longdesc',
	'file_ssurl',
	'file_dlurl',
	'file_time',
	'file_catid',
	'file_posticon',
	'file_license',
	'file_dls',
	'file_last',
	'file_pin',
	'file_docsurl',
	'file_rating',
	'file_totalvotes'),
'pa_license' => array(
	'license_id',
	'license_name',
	'license_text'),
'pa_settings' => array(
	'settings_id',
	'settings_dbname',
	'settings_sitename',
	'settings_dbdescription',
	'settings_dburl',
	'settings_topnumber',
	'settings_homeurl',
	'settings_newdays',
	'settings_stats',
	'settings_viewall',
	'settings_showss',
	'settings_disable',
	'allow_html',
	'allow_bbcode',
	'allow_smilies',
	'allow_comment_links',
	'no_comment_link_message',
	'allow_comment_images',
	'no_comment_image_message',
	'max_comment_chars',
	'directly_linked'),
'pa_votes' => array(
	'votes_ip',
	'votes_file'),
'portal_config' => array(
	'config_name',
	'config_value'),
'posts' => array(
	'post_id',
	'topic_id',
	'forum_id',
	'poster_id',
	'post_time',
	'post_start_time',
	'poster_ip',
	'post_username',
	'enable_bbcode',
	'enable_html',
	'enable_smilies',
	'enable_sig',
	'post_edit_time',
	'post_edit_count',
	'post_attachment',
	'user_agent',
	'post_icon',
	'post_expire',
	'reporter_id',
	'post_marked',
	'post_approve',
	'poster_delete',
	'post_edit_by',
	'post_parent',
	'post_order'),
'posts_text' => array(
	'post_id',
	'bbcode_uid',
	'post_subject',
	'post_text'),
'posts_text_history' => array(
	'th_id',
	'th_post_id',
	'th_post_text',
	'th_user_id',
	'th_time'),
'privmsgs' => array(
	'privmsgs_id',
	'privmsgs_type',
	'privmsgs_subject',
	'privmsgs_from_userid',
	'privmsgs_to_userid',
	'privmsgs_date',
	'privmsgs_ip',
	'privmsgs_enable_bbcode',
	'privmsgs_enable_html',
	'privmsgs_enable_smilies',
	'privmsgs_attach_sig',
	'privmsgs_attachment'),
'privmsgs_text' => array(
	'privmsgs_text_id',
	'privmsgs_bbcode_uid',
	'privmsgs_text'),
'quota_limits' => array(
	'quota_limit_id',
	'quota_desc',
	'quota_limit'),
'ranks' => array(
	'rank_id',
	'rank_title',
	'rank_min',
	'rank_special',
	'rank_image',
	'rank_group'),
'read_history' => array(
	'user_id',
	'post_id'),
'search_results' => array(
	'search_id',
	'session_id',
	'search_array',
	'search_time'),
'search_wordlist' => array(
	'word_text',
	'word_id',
	'word_common'),
'search_wordmatch' => array(
	'post_id',
	'word_id',
	'title_match'),
'sessions' => array(
	'session_id',
	'session_user_id',
	'session_start',
	'session_time',
	'session_ip',
	'session_page',
	'session_logged_in',
	'session_admin'),
'sessions_keys' => array(
	'key_id',
	'user_id',
	'last_ip',
	'last_login'),
'shoutbox' => array(
	'id',
	'sb_user_id',
	'msg',
	'timestamp'),
'shoutbox_config' => array(
	'config_name',
	'config_value'),
'smilies' => array(
	'smilies_id',
	'code',
	'smile_url',
	'emoticon',
	'smile_order'),
'stats_config' => array(
	'config_name',
	'config_value'),
'stats_modules' => array(
	'module_id',
	'name',
	'active',
	'installed',
	'display_order',
	'update_time',
	'auth_value',
	'module_info_cache',
	'module_db_cache',
	'module_result_cache',
	'module_info_time',
	'module_cache_time'),
'themes' => array(
	'themes_id',
	'template_name',
	'style_name',
	'head_stylesheet',
	'body_background',
	'body_bgcolor',
	'body_text',
	'body_link',
	'body_vlink',
	'body_alink',
	'body_hlink',
	'tr_color1',
	'tr_color2',
	'tr_color3',
	'tr_color_helped',
	'tr_class1',
	'tr_class2',
	'tr_class3',
	'th_color1',
	'th_color2',
	'th_color3',
	'th_class1',
	'th_class2',
	'th_class3',
	'td_color1',
	'td_color2',
	'td_color3',
	'td_class1',
	'td_class2',
	'td_class3',
	'fontface1',
	'fontface2',
	'fontface3',
	'fontsize1',
	'fontsize2',
	'fontsize3',
	'fontcolor1',
	'fontcolor2',
	'fontcolor3',
	'fontcolor_admin',
	'fontcolor_jradmin',
	'fontcolor_mod',
	'factive_color',
	'faonmouse_color',
	'faonmouse2_color',
	'span_class1',
	'span_class2',
	'span_class3',
	'img_size_poll',
	'img_size_privmsg'),
'themes_name' => array(
	'themes_id',
	'tr_color1_name',
	'tr_color2_name',
	'tr_color3_name',
	'tr_class1_name',
	'tr_class2_name',
	'tr_class3_name',
	'th_color1_name',
	'th_color2_name',
	'th_color3_name',
	'th_class1_name',
	'th_class2_name',
	'th_class3_name',
	'td_color1_name',
	'td_color2_name',
	'td_color3_name',
	'td_class1_name',
	'td_class2_name',
	'td_class3_name',
	'fontface1_name',
	'fontface2_name',
	'fontface3_name',
	'fontsize1_name',
	'fontsize2_name',
	'fontsize3_name',
	'fontcolor1_name',
	'fontcolor2_name',
	'fontcolor3_name',
	'span_class1_name',
	'span_class2_name',
	'span_class3_name'),
'topics' => array(
	'topic_id',
	'forum_id',
	'topic_title',
	'topic_poster',
	'topic_time',
	'topic_views',
	'topic_replies',
	'topic_status',
	'topic_vote',
	'topic_type',
	'topic_first_post_id',
	'topic_last_post_id',
	'topic_moved_id',
	'topic_attachment',
	'topic_icon',
	'topic_expire',
	'topic_color',
	'topic_title_e',
	'topic_action',
	'topic_action_user',
	'topic_action_date',
	'topic_tree_width',
	'topic_accept'),
'topics_ignore' => array(
	'topic_id',
	'user_id'),
'topics_watch' => array(
	'topic_id',
	'user_id',
	'notify_status'),
'topic_view' => array(
	'topic_id',
	'user_id',
	'view_time',
	'view_count'),
'users' => array(
	'user_id',
	'user_active',
	'username',
	'user_password',
	'user_session_time',
	'user_session_page',
	'user_lastvisit',
	'user_regdate',
	'user_level',
	'user_posts',
	'user_timezone',
	'user_style',
	'user_lang',
	'user_new_privmsg',
	'user_unread_privmsg',
	'user_last_privmsg',
	'user_emailtime',
	'user_viewemail',
	'user_viewaim',
	'user_attachsig',
	'user_allowhtml',
	'user_allowbbcode',
	'user_allowsmile',
	'user_allowavatar',
	'user_allowsig',
	'user_allow_pm',
	'user_allow_viewonline',
	'user_notify',
	'user_notify_pm',
	'user_popup_pm',
	'user_rank',
	'user_avatar',
	'user_avatar_type',
	'user_email',
	'user_icq',
	'user_website',
	'user_from',
	'user_sig',
	'user_sig_bbcode_uid',
	'user_sig_image',
	'user_aim',
	'user_yim',
	'user_msnm',
	'user_occ',
	'user_interests',
	'user_actkey',
	'user_newpasswd',
	'user_birthday',
	'user_next_birthday_greeting',
	'user_custom_rank',
	'user_photo',
	'user_photo_type',
	'user_custom_color',
	'user_badlogin',
	'user_blocktime',
	'user_block_by',
	'disallow_forums',
	'can_custom_ranks',
	'can_custom_color',
	'user_gender',
	'can_topic_color',
	'allowpm',
	'no_report_popup',
	'refresh_report_popup',
	'no_report_mail',
	'user_avatar_width',
	'user_avatar_height',
	'special_rank',
	'user_allow_helped',
	'user_ip',
	'user_ip_login_check',
	'user_spend_time',
	'user_visit',
	'user_session_start',
	'read_tracking_last_update',
	'user_jr'),
'user_group' => array(
	'group_id',
	'user_id',
	'user_pending'),
'users_warnings' => array(
	'id',
	'userid',
	'modid',
	'date',
	'value',
	'reason',
	'archive',
	'warning_viewed'),
'vote_desc' => array(
	'vote_id',
	'topic_id',
	'vote_text',
	'vote_start',
	'vote_length',
	'vote_max',
	'vote_voted',
	'vote_hide',
	'vote_tothide'),
'vote_results' => array(
	'vote_id',
	'vote_option_id',
	'vote_option_text',
	'vote_result'),
'vote_voters' => array(
	'vote_id',
	'vote_user_id',
	'vote_user_ip',
	'vote_cast'),
'words' => array(
	'word_id',
	'word',
	'replacement'),
);

$config_inserts = array(

'attachments_config' => array(
	'upload_dir',
	'upload_img',
	'topic_icon',
	'display_order',
	'max_filesize',
	'attachment_quota',
	'max_filesize_pm',
	'max_attachments',
	'max_attachments_pm',
	'disable_mod',
	'allow_pm_attach',
	'attachment_topic_review',
	'allow_ftp_upload',
	'show_apcp',
	'ftp_server',
	'ftp_path',
	'download_path',
	'ftp_user',
	'ftp_pass',
	'img_display_inlined',
	'img_max_width',
	'img_max_height',
	'img_link_width',
	'img_link_height',
	'img_create_thumbnail',
	'img_min_thumb_filesize',
	'img_imagick',
	'default_upload_quota',
	'default_pm_quota',
	'ftp_pasv_mode',
	'use_gd2'),
'config' => array(
	'server_name',
	'server_port',
	'script_path',
	'cookie_domain',
	'cookie_name',
	'cookie_path',
	'cookie_secure',
	'board_disable',
	'session_length',
	'check_address',
	'sitename',
	'site_desc',
	'allow_html',
	'allow_html_tags',
	'allow_bbcode',
	'allow_smilies',
	'allow_sig',
	'allow_namechange',
	'allow_avatar_local',
	'allow_avatar_remote',
	'allow_avatar_upload',
	'override_user_style',
	'posts_per_page',
	'topics_per_page',
	'hot_threshold',
	'max_poll_options',
	'max_sig_chars',
	'max_inbox_privmsgs',
	'max_sentbox_privmsgs',
	'max_savebox_privmsgs',
	'board_email_sig',
	'board_email',
	'smtp_delivery',
	'smtp_host',
	'smtp_username',
	'smtp_password',
	'require_activation',
	'flood_interval',
	'board_email_form',
	'avatar_filesize',
	'avatar_max_width',
	'avatar_max_height',
	'avatar_path',
	'avatar_gallery_path',
	'smilies_path',
	'default_style',
	'default_dateformat',
	'board_timezone',
	'prune_enable',
	'privmsg_disable',
	'gzip_compress',
	'coppa_fax',
	'coppa_mail',
	'record_online_users',
	'record_online_date',
	'version',
	'allow_autologin',
	'allow_photo_remote',
	'allow_photo_upload',
	'photo_filesize',
	'photo_max_height',
	'photo_max_width',
	'photo_path',
	'allow_custom_rank',
	'birthday_greeting',
	'max_user_age',
	'min_user_age',
	'birthday_check_day',
	'cload',
	'cchat',
	'cstat',
	'cregist',
	'cstyles',
	'ccount',
	'cchat2',
	'cbirth',
	'cpost',
	'ctop',
	'cfriend',
	'cage',
	'cjoin',
	'cfrom',
	'cposts',
	'clevell',
	'cleveld',
	'cignore',
	'cquick',
	'csearch',
	'cicq',
	'cllogin',
	'clevelp',
	'cyahoo',
	'cmsn',
	'cjob',
	'cinter',
	'cemail',
	'cbbcode',
	'chtml',
	'csmiles',
	'clang',
	'ctimezone',
	'cbstyle',
	'refresh',
	'meta_keywords',
	'meta_description',
	'cavatar',
	'clog',
	'cagent',
	'login_require',
	'crestrict',
	'validate',
	'button_b',
	'button_i',
	'button_u',
	'button_q',
	'button_c',
	'button_l',
	'button_im',
	'button_ur',
	'button_ce',
	'button_f',
	'button_s',
	'button_hi',
	'color_box',
	'size_box',
	'glow_box',
	'freak',
	'allow_bbcode_quest',
	'sql',
	'cregist_b',
	'allow_custom_color',
	'custom_color_view',
	'custom_color_use',
	'post_icon',
	'auto_date',
	'newest',
	'download',
	'ipview',
	'show_badwords',
	'album_gallery',
	'address_whois',
	'u_o_t_d',
	'expire',
	'expire_value',
	'numer_gg',
	'haslo_gg',
	'block_time',
	'max_login_error',
	'min_password_len',
	'force_complex_password',
	'password_not_login',
	'del_user_notify',
	'require_aim',
	'require_website',
	'require_location',
	'post_footer',
	'graphic',
	'max_sig_custom_rank',
	'max_sig_location',
	'custom_color_mod',
	'custom_rank_mod',
	'allow_sig_image',
	'sig_images_path',
	'sig_image_filesize',
	'sig_image_max_width',
	'sig_image_max_height',
	'hide_viewed_admin',
	'hide_edited_admin',
	'who_viewed',
	'who_viewed_admin',
	'edit_time',
	'gender',
	'require_gender',
	'main_admin_id',
	'day_to_prune',
	'banner_top',
	'banner_top_enable',
	'banner_bottom',
	'banner_bottom_enable',
	'header_enable',
	'not_edit_admin',
	'staff_forums',
	'staff_enable',
	'smilies_columns',
	'smilies_rows',
	'smilies_w_columns',
	'generate_time',
	'name_color',
	'desc_color',
	'mod_nick_color',
	'warnings_enable',
	'mod_warnings',
	'mod_edit_warnings',
	'mod_value_warning',
	'write_warnings',
	'ban_warnings',
	'expire_warnings',
	'warnings_mods_public',
	'viewtopic_warnings',
	'board_msg_enable',
	'board_msg',
	'width_forum',
	'width_table',
	'width_color1',
	'width_color2',
	'table_border',
	'rebuild_search',
	'generate_time_admin',
	'r_a_r_time',
	'visitors',
	'email_return_path',
	'email_from',
	'poster_posts',
	'sub_forum',
	'sub_forum_over',
	'split_cat',
	'split_cat_over',
	'last_topic_title',
	'last_topic_title_over',
	'last_topic_title_length',
	'sub_level_links',
	'sub_level_links_over',
	'display_viewonline',
	'display_viewonline_over',
	'ignore_topics',
	'topic_color',
	'topic_color_all',
	'topic_color_mod',
	'allow_sig_image_img',
	'last_dtable_notify',
	'report_no_guestes',
	'report_no_auth_users',
	'report_no_auth_groups',
	'report_disabled_users',
	'report_disabled_groups',
	'report_only_admin',
	'report_popup_height',
	'report_popup_width',
	'report_popup_links_target',
	'report_disable',
	'sendmail_fix',
	'allow_avatar',
	'last_visitors_time',
	'max_sig_chars_admin',
	'max_sig_chars_mod',
	'viewonline',
	'restrict_smilies',
	'topic_preview',
	'not_anonymous_posting',
	'not_anonymous_quickreply',
	'max_smilies',
	'portal_link',
	'search_enable',
	'overlib',
	'admin_notify_reply',
	'admin_notify_message',
	'topic_start_date',
	'topic_start_dateformat',
	'autorepair_tables',
	'echange_banner',
	'banners_list',
	'split_messages',
	'split_messages_admin',
	'split_messages_mod',
	'admin_html',
	'jr_admin_html',
	'mod_html',
	'helped',
	'del_notify_method',
	'del_notify_enable',
	'del_notify_choice',
	'open_in_windows',
	'title_explain',
	'show_action_unlocked',
	'show_action_locked',
	'show_action_moved',
	'show_action_expired',
	'show_action_edited_by_others',
	'show_action_edited_self',
	'show_action_edited_self_all',
	'allow_mod_delete_actions',
	'show_rules',
	'mod_spy',
	'mod_spy_admin',
	'post_overlib',
	'ph_days',
	'ph_len',
	'ph_mod',
	'ph_mod_delete',
	'newestuser',
	'topiccount',
	'postcount',
	'usercount',
	'lastpost',
	'anonymous_simple',
	'onmouse',
	'birthday_data',
	'data',
	'last_resync',
	'advert',
	'advert_foot',
	'view_ad_by',
	'advert_width',
	'advert_separator',
	'advert_separator_l',
	'adv_person_time',
	'group_rank_hack_version',
	'disable_type'),
'shoutbox_config' => array(
	'allow_guest_view',
	'allow_guest',
	'allow_users_view',
	'allow_users',
	'allow_delete_all',
	'allow_delete',
	'allow_delete_m',
	'allow_edit_all',
	'allow_edit',
	'allow_edit_m',
	'allow_bbcode',
	'allow_smilies',
	'links_names',
	'make_links',
	'count_msg',
	'delete_days',
	'text_lenght',
	'word_lenght',
	'date_format',
	'date_on',
	'shoutbox_on',
	'shout_width',
	'shout_height',
	'banned_user_id',
	'banned_user_id_view'),
);

?>