<?php
/*
  paFileDB 3.0
  ę2001/2002 PHP Arena
  Written by Todd
  todd@phparena.net
  http://www.phparena.net
  Keep all copyright links on the script visible
  Please read the license included with this script for more information.
*/

if ( @$_GET['category'] == 'edit' || @$_POST['category'] == 'edit' )
{
	define('MODULE_ID', 64);
}
else if ( @$_GET['category'] == 'delete' || @$_POST['category'] == 'delete' )
{
	define('MODULE_ID', 65);
}
if ( @$_GET['category'] == 'order' || @$_POST['category'] == 'order' )
{
	define('MODULE_ID', 66);
}
else
{
	define('MODULE_ID', 41);
}

define('IN_PHPBB', 1);

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['Download2']['Acat'] = "$file?category=add";
	$module['Download2']['Ecat'] = "$file?category=edit";
	$module['Download2']['Dcat'] = "$file?category=delete";
	$module['Download2']['Rcat'] = "$file?category=order";
	return;
}

$phpbb_root_path = "./../";

require($phpbb_root_path . 'extension.inc');

require('./pagestart.' . $phpEx);

include($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_admin_pafiledb.' . $phpEx);

include($phpbb_root_path . 'pafiledb/includes/functions.' . $phpEx); 

$config = pafiledb_config();

if( isset($_GET['category']) || isset($_POST['category']) )
{

	$category = (isset($_POST['category'])) ? $_POST['category'] : $_GET['category'];

	switch($category)
	{

		case 'add':
		{

			$template->set_filenames(array(
				'admin' => 'admin/pa_admin_cat_add.tpl')
			);

			if ( isset($_GET['add']) || isset($_POST['add']) )
			{
				$add = ( isset($_GET['add']) ) ? $_GET['add'] : $_POST['add'];
			}

			if ($add == 'do')
			{
				if ( isset($_GET['form']) || isset($_POST['form']) )
				{
					$form = ( isset($_GET['form']) ) ? $_GET['form'] : $_POST['form'];
				}

				$sql = "INSERT INTO " . PA_CATEGORY_TABLE . " (cat_name, cat_desc, cat_files, cat_1xid, cat_parent, cat_order) VALUES('" . $form['name'] . "', '" . $form['description'] . "', '0', '0', '" . $form['parent'] . "', '0')";

				if ( !($db->sql_query($sql)) )
				{
					message_die(GENERAL_ERROR, 'Couldnt add the settings into the database', '', __LINE__, __FILE__, $sql);
				}

				$cid = $db->sql_nextid();

				$sql = "UPDATE " . PA_CATEGORY_TABLE . " SET cat_order = '" . $cid . "' WHERE cat_id = '" . $cid . "'";

				if ( !($db->sql_query($sql)) )
				{
					message_die(GENERAL_ERROR, 'Couldnt Update the settings into the database', '', __LINE__, __FILE__, $sql);
				}

				$message = $lang['Catadded'] . '<br /><br />' . sprintf($lang['Click_return'], '<a href="' . append_sid("admin_category.$phpEx?category=add") . '">', '</a>') . '<br /><br />' . sprintf($lang['Click_return_admin_index'], '<a href="' . append_sid("index.$phpEx?pane=right") . '">', '</a>');

				message_die(GENERAL_MESSAGE, $message);
			}

			if (empty($add)) 
			{
				$dropmenu .= jumpmenu('', 'admin', '');

				$template->assign_vars(array(
					'S_ADD_CAT_ACTION' => append_sid("admin_category.$phpEx"),
					'L_ACATTITLE' => $lang['Acattitle'],
					'L_CATEXPLAIN' => $lang['Catexplain'],
					'L_CATNAME' => $lang['Catname'],
					'L_CATNAMEINFO' => $lang['Catnameinfo'],
					'L_CATDESC' => $lang['Catdesc'],
					'L_CATDESCINFO' => $lang['Catdescinfo'],
					'L_CATPARENT' => $lang['Catparent'],
					'L_NONE' => $lang['None'],
					'DROPMENU' => $dropmenu,
					'L_CATPARENTINFO' => $lang['Catparentinfo'])
				); 
			}

			$template->pparse('admin');

			break;
		}

		case 'edit':
		{

			$template->set_filenames(array(
				'admin' => 'admin/pa_admin_cat_edit.tpl')
			);

			if ( isset($_GET['edit']) || isset($_POST['edit']) )
			{
				$edit = ( isset($_GET['edit']) ) ? $_GET['edit'] : $_POST['edit'];
			}

			if ($edit == 'do')
			{

				if ( isset($_GET['form']) || isset($_POST['form']) )
				{
					$form = ( isset($_GET['form']) ) ? $_GET['form'] : $_POST['form'];
				}

				if ( isset($_GET['id']) || isset($_POST['id']) )
				{
					$id = ( isset($_GET['id']) ) ? intval($_GET['id']) : intval($_POST['id']);
				}

				$sql = "UPDATE " . PA_CATEGORY_TABLE . " SET cat_name = '" . $form['name'] . "', cat_desc = '" . $form['description'] . "', cat_parent = '" . $form['parent'] . "' WHERE cat_id = '" . $id . "'";

				if ( !($result = $db->sql_query($sql)) )
				{
					message_die(GENERAL_ERROR, 'Couldnt Query Info', '', __LINE__, __FILE__, $sql);
				}

				$message = $lang['Catedited'] . '<br /><br />' . sprintf($lang['Click_return'], '<a href="' . append_sid("admin_category.$phpEx?category=edit") . '">', '</a>') . '<br /><br />' . sprintf($lang['Click_return_admin_index'], '<a href="' . append_sid("index.$phpEx?pane=right") . '">', '</a>');

				message_die(GENERAL_MESSAGE, $message);
			}

			if ($edit == 'form')
			{

				if ( isset($_GET['select']) || isset($_POST['select']) )
				{
					$select = ( isset($_GET['select']) ) ? $_GET['select'] : $_POST['select'];
				}

				$sql = "SELECT * FROM " . PA_CATEGORY_TABLE . " WHERE cat_id = '" . $select . "'";

				if ( !($result = $db->sql_query($sql)) )
				{
					message_die(GENERAL_ERROR, 'Couldnt Query Info', '', __LINE__, __FILE__, $sql);
				}

				$cat = $db->sql_fetchrow($result);

				if ($cat['cat_parent'] == 0) 
				{
					$dropmenu .= '<option value="0" selected>' . $lang['None'] . '</option>\n';
				}
				else
				{
					$dropmenu .= '<option value="0">' . $lang['None'] . '</option>\n';
				}

				$dropmenu .= jumpmenu('', 'admin', array($cat['cat_parent']=>1));

				$template->assign_block_vars("edit_form", array());

				$template->assign_vars(array(
					'S_EDIT_CAT_ACTION' => append_sid("admin_category.$phpEx"),
					'L_ECATTITLE' => $lang['Ecattitle'],
					'L_CATEXPLAIN' => $lang['Catexplain'],
					'CAT_NAME' => $cat['cat_name'],
					'CAT_DESC' => $cat['cat_desc'],
					'L_CATNAME' => $lang['Catname'],
					'L_CATNAMEINFO' => $lang['Catnameinfo'],
					'L_CATDESC' => $lang['Catdesc'],
					'L_CATDESCINFO' => $lang['Catdescinfo'],
					'L_CATPARENT' => $lang['Catparent'],
					'SELECT' => $select,
					'DROPMENU' => $dropmenu,
					'L_CATPARENTINFO' => $lang['Catparentinfo'])
				);

			}

			if (empty($edit))
			{

				$row = '<tr><td colspan="2" class="row1" align="center" valign="middle">'
					.'<select name="select" class="forminput">'
					.jumpmenu('', 'admin', '')
					.'</select>'
					.'</td></tr>';

				$template->assign_block_vars("edit", array());

				$template->assign_vars(array(
					'S_EDIT_CAT_ACTION' => append_sid("admin_category.$phpEx"),
					'L_ECATTITLE' => $lang['Ecattitle'],
					'L_CATEXPLAIN' => $lang['Catexplain'],
					'ROW' => $row)
				);
			}

			$template->pparse('admin');

			break;
		} 

		case 'delete':
		{

			$template->set_filenames(array(
				'admin' => 'admin/pa_admin_cat_delete.tpl')
			);

			if ( isset($_GET['delete']) || isset($_POST['delete']) )
			{
				$delete = ( isset($_GET['delete']) ) ? $_GET['delete'] : $_POST['delete'];
			}

			if ($delete == 'do')
			{

				if ( isset($_GET['select']) || isset($_POST['select']) )
				{
					$select = ( isset($_GET['select']) ) ? $_GET['select'] : $_POST['select'];
				}

				if (empty($select))
				{
					$message = $lang['Cdelerror'] . '<br /><br />' . sprintf($lang['Click_return'], '<a href="' . append_sid("admin_category.$phpEx?category=delete") . '">', '</a>');

					message_die(GENERAL_MESSAGE, $message);
				}
				else
				{

					foreach ($select as $id)
					{
						$sql = "DELETE FROM " . PA_CATEGORY_TABLE . " WHERE cat_id = '" . $id . "'";

						if ( !($db->sql_query($sql)) )
						{
							message_die(GENERAL_ERROR, 'Couldnt Query Info', '', __LINE__, __FILE__, $sql);
						}

						$sql = "DELETE FROM " . PA_CATEGORY_TABLE . " WHERE cat_parent = '" . $id . "'";

						if ( !($db->sql_query($sql)) )
						{
							message_die(GENERAL_ERROR, 'Couldnt Query Info', '', __LINE__, __FILE__, $sql);
						}

						if ( isset($_GET['delfiles']) || isset($_POST['delfiles']) )
						{
							$delfiles = ( isset($_GET['delfiles']) ) ? $_GET['delfiles'] : $_POST['delfiles'];
						}

						if ($delfiles == 'yes')
						{
							$sql = "DELETE FROM " . PA_FILES_TABLE . " WHERE file_catid = '$id'";

							if ( !($db->sql_query($sql)) )
							{
								message_die(GENERAL_ERROR, 'Couldnt Query Info', '', __LINE__, __FILE__, $sql);							}

						}
					}

					$message = $lang['Catsdeleted'] . '<br /><br />' . sprintf($lang['Click_return'], '<a href="' . append_sid("admin_category.$phpEx?category=delete") . '">', '</a>') . '<br /><br />' . sprintf($lang['Click_return_admin_index'], '<a href="' . append_sid("index.$phpEx?pane=right") . '">', '</a>');

					message_die(GENERAL_MESSAGE, $message);
				}
	
			}

			if (empty($delete))
			{

				$row = '<tr><td colspan="2" class="row1" align="center" valign="middle">'
					.'<select name="select[]" multiple="multiple"size="4" class="forminput">'
					.jumpmenu('', 'admin', '')
					.'</select>'
					.'</td></tr>';

					$template->assign_vars(array(
						'S_DELETE_CAT_ACTION' => append_sid("admin_category.$phpEx"),
						'L_DCATTITLE' => $lang['Dcattitle'],
						'L_CATEXPLAIN' => $lang['Catexplain'],
						'L_DELETE'=> $lang['Delete'],
						'ROW' => $row,
						'L_DELFILES' => $lang['Delfiles'],
						'L_YES' => $lang['Yes'],
						'L_NO' => $lang['No'])
					);
			}

			$template->pparse('admin');

			break;
		} 

		case 'order':
		{

			$template->set_filenames(array(
				'admin' => 'admin/pa_admin_cat_order.tpl')
			);

			if ( isset($_GET['order']) || isset($_POST['order']) )
			{
				$order = ( isset($_GET['order']) ) ? $_GET['order'] : $_POST['order'];
			}

			if ($order == 'do')
			{

				if ( isset($_GET['num']) || isset($_POST['num']) )
				{
					$num = ( isset($_GET['num']) ) ? $_GET['num'] : $_POST['num'];
				}

				foreach($num as $key => $value)
				{
					$sql = "UPDATE " . PA_CATEGORY_TABLE . " SET cat_order = '$value' WHERE cat_id = '$key'";

					if ( !($db->sql_query($sql)) )
					{
						message_die(GENERAL_ERROR, 'Couldnt Query Info', '', __LINE__, __FILE__, $sql);
					}
				}

				$message = $lang['Rcatdone'] . '<br /><br />' . sprintf($lang['Click_return'], '<a href="' . append_sid("admin_category.$phpEx?category=order") . '">', '</a>') . '<br /><br />' . sprintf($lang['Click_return_admin_index'], '<a href="' . append_sid("index.$phpEx?pane=right") . '">', '</a>');

				message_die(GENERAL_MESSAGE, $message);
		
			}

			if (empty($order))
			{
				$rows = '';
				if ( isset($_GET['id']) || isset($_POST['id']) )
				{
					$id = ( isset($_GET['id']) ) ? intval($_GET['id']) : intval($_POST['id']);
				}

				if (!$id)
				{
					$id = 0;
				}

				$sql = "SELECT * FROM " . PA_CATEGORY_TABLE . " WHERE cat_parent = '" . $id . "' ORDER BY cat_order ASC";

				if ( !($result = $db->sql_query($sql)) )
				{
					message_die(GENERAL_ERROR, 'Couldnt Query Info', '', __LINE__, __FILE__, $sql);	
				}

				while ($cat = $db->sql_fetchrow($result)) 
				{
					$rows .= '<tr><td width="5%" class="row1" align="center" valign="middle"><input type="text" name="num[' . $cat['cat_id'] . ']" value="' . $cat['cat_order'] . '" size="2"></td>
						<td width="95%" class="row1"><span class="cattitle"><a href="' . append_sid("admin_category.php?category=order&amp;id=" . $cat['cat_id']) . '">' . $cat['cat_name'] . '</a></span><br><span class="gensmall">' . $cat['cat_desc'] . '</span></td></tr>';
				}

				$template->assign_vars(array(
					'S_ORDER_CAT_ACTION' => append_sid("admin_category.$phpEx"),
					'L_RCATTITLE' => $lang['Rcattitle'],
					'L_RCATEXPLAIN' => $lang['Rcatexplain'],
					'L_RCATINFO' => $lang['Rcatinfo'],
					'ROW' => $rows)
				);
			}

			$template->pparse('admin');

			break; 
		}
	}
}

include('./page_footer_admin.'.$phpEx);

?>