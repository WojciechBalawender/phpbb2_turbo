<?php
$lang['no_admin'] = 'Nie jesteś administratorem.';
$lang['No_query_info'] = 'Ważne jest aby sprawdzić powód niewykonania instrukcji. Prawidłowym objawem niewykonania instrukcji jest istnienie już w bazie lub brak takiej tabeli lub kolumny w tabeli. Nieprawidłowym objawem jest jedynie informacja o braku dostępu do komendy lub błąd w składni instrukcji SQL (syntax). W razie wątpliwości można poszukać odpowiedzi lub zadać pytanie na forum: <a href="http://www.przemo.org/phpBB2/" tatger="_blank">http://www.przemo.org/phpBB2/</a> wklejając linię która nie została wykonana. Uwaga nie należy korzystać z pliku update.sql do wykonania aktualizacji "ręcznie" gdyż nie zawiera on wszystkich koniecznych instrukcji !';
$lang['update_body'] = '<b>Modyfikacja forum do wersji phpBB modified v1.12.8 by Przemo</b></span><br /><span class="gensmall">Użyj aby zamienić oryginalne phpBB 2.0.* lub uaktualnić z dowolnej wersji phpBB modified by Przemo z przedziału: 1.5 - 1.12.7</span></td></tr>
	<tr><td class="row2"><span class="gensmall">
	<b>Instrukcja:</b><br />
	Przed przystąpieniem do aktualizacji, na wszelki wypadek zrób kopię swojego forum. Skopiuj wszystkie pliki, oraz zrób kopię bazy danych,
	w Panelu Admina, lub w PhpMyAdminie.<br />
	Gdy masz już kopię forum, nie nadpisujesz jeszcze plików forum, plikami aktualizacji, tylko najpierw uruchamiasz aktualizację (przycisk niżej)
	<br />Teraz naciśnij przycisk <b>"Zacznij aktualizację"</b> 
	Zostaną wyświetlone instrukcje które nie zostały wykonane, zarówno w aktualizacji z oryginalnego phpBB jak i aktualizacji z niższej wersji phpBB Turbo UTF-8 wiele z tych instrukcji nie zostanie wykonanych gdyż jest to uniwersalny aktualizator. ' . $lang['No_query_info'] . '<br />
	Jeżeli operacja będzie trwać za długo i serwer zatrzyma skrypt, możesz wykonać aktualizację ponownie (odświeżyć stronę) kolejne czynności będą wykonywane.<br />
	Po zakończeniu nadpisz pliki swojego forum, plikami mojej wersji. Musi być taka kolejność, najpierw włączenie aktualizacji potem nadpisanie plików. W odwrotnej kolejności, trzeba być cały czas zalogowanym jako admin przy kopiowaniu plików, co może sie nie udać i wówczas trzeba skorzystać z kopii swojego forum :)
	<br /><br />Życzę powodzenia.';
$lang['start_update'] = 'Zacznij aktualizację';
$lang['checksum_error'] = 'Nieprawidłowa suma kontrolna pliku <b>%s</b> ! (%s)<br />Spróbuj jeszcze raz skopiować plik na serwer.';
$lang['result'] = '<b>Wynik aktualizacji bazy do wersji phpBB modified v1.12.8 by Przemo</b><br />Instrukcji wykonanych: <b>%s</b>, niewykonanych: <b>%s</b><br /><br />&bull;Sprawdź poniższe instrukcje SQL<br /><span class="gensmall">' . $lang['No_query_info'] . '</span><br /><br />&bull; Nadpisz wszystkie pliki  oprócz <i><b>config.php</b></i><br />&bull; Sprawdź w Panelu Admina: Kontrolę Systemu, zwróć uwagę na prawa katalogów do zapisu<br />&bull; Jeżeli aktualizujesz z oryginalnego phpBB popraw w Panelu Admina i przenieś do katalogu stylu obrazki rang<br />&bull; <span class="gensmall">Użyj pliku <a href="update_useragent.php" target="_blank">/scripts/update_useragent.php</a> aby dostosować ikony przeglądarek dla "starych" postów (użyj po wysłaniu nowych plików) </span><br />&bull; Dopasuj nowe kolory Admina, JR Admina, Moderatorów, kolorów OnMouseOver w Panelu Admina w edycji danych stylu';
$lang['failed'] = 'Niewykonane';
$lang['query_ok'] = 'Wykonane';
$lang['No_available_db'] = 'phpBB Turbo UTF-8 obsługuje tylko bazę danych MySQL. Aktualne forum korzysta z innej bazy danych.';

$lang['UA_time_exc'] = 'Czas generowania strony został przekroczony. <br />Zostało przetworzonych postów: <b>%s</b> z <b>%s</b> <br />Kontynuuj konwersję: %sDalej%s.';
$lang['UA_title'] = 'Aktualizacja identyfikatora przeglądarki';
$lang['UA_finished'] = 'Aktualizacja została zakończona. <br />Zaktualizowano postów: <b>%s</b>';
$lang['UA_no_useragent'] = 'Nie można odnaleźć katalogu z ikonami systemów i przeglądarek. <br />Sprawdź, czy katalog <i>templates/Silueta/images/user_agent</i> istnieje. Jeśli nie, skopiuj go ze ściągniętej paczki. <br />Jeśli istnieje, a pojawia się ten komunikat, zgłoś to nam na forum <a href="http://www.przemo.org/phpBB2/forum/">http://www.przemo.org/phpBB2/forum/</a>.';
$lang['Generate_file'] = 'Zaznacz aby tylko wygenerować plik z zapytaniami do bazy<br />UWAGA nie wszystkie zapytania zostają wykonane, tak więc "ręczne" wykonanie zapytań może być kłopotliwe';
$lang['dangerous_files'] = 'UWAGA: skrypt wykrył na serwerze pliki, które mogą być niebezpieczne. Ponieważ poniższe pliki nie powinny się znajdować w tych miejscach, zalecane jest aby szczegółowo przejrzeć każdy z nich i upewnić się, że nie zawiera on exploita lub innego niebezpiecznego kodu. W przypadku wątpliwości, zgłoś to nam na forum <a href="http://www.przemo.org/phpBB2/forum/">http://www.przemo.org/phpBB2/forum/</a>. Jeśli masz pełną świadomość, że poniższe pliki należą do Ciebie i nie są niebezpieczne, kontynuuj aktualizację';
?>