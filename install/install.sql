-- Import zakończony sukcesem, wykonano 300 zapytań. (install.sql)

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Struktura tabeli dla tabeli `phpbb_advertisement`
--

CREATE TABLE `phpbb_advertisement` (
  `id` mediumint(9) NOT NULL,
  `html` text,
  `email` varchar(128) DEFAULT '',
  `clicks` int(9) NOT NULL DEFAULT '0',
  `position` tinyint(1) NOT NULL DEFAULT '0',
  `porder` mediumint(4) NOT NULL DEFAULT '0',
  `added` int(11) NOT NULL DEFAULT '0',
  `expire` int(11) NOT NULL DEFAULT '0',
  `last_update` int(11) NOT NULL DEFAULT '0',
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_adv_person`
--

CREATE TABLE `phpbb_adv_person` (
  `user_id` mediumint(9) NOT NULL DEFAULT '0',
  `person_id` mediumint(9) NOT NULL DEFAULT '0',
  `person_ip` char(8) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_anti_robotic_reg`
--

CREATE TABLE `phpbb_anti_robotic_reg` (
  `session_id` char(32) NOT NULL DEFAULT '',
  `reg_key` char(4) NOT NULL DEFAULT '',
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_attachments`
--

CREATE TABLE `phpbb_attachments` (
  `attach_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `post_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `privmsgs_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `user_id_1` mediumint(8) NOT NULL DEFAULT '0',
  `user_id_2` mediumint(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_attachments_config`
--

CREATE TABLE `phpbb_attachments_config` (
  `config_name` varchar(255) NOT NULL DEFAULT '',
  `config_value` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_attachments_desc`
--

CREATE TABLE `phpbb_attachments_desc` (
  `attach_id` mediumint(8) UNSIGNED NOT NULL,
  `physical_filename` varchar(255) NOT NULL DEFAULT '',
  `real_filename` varchar(255) NOT NULL DEFAULT '',
  `download_count` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `comment` varchar(255) DEFAULT NULL,
  `extension` varchar(100) DEFAULT NULL,
  `mimetype` varchar(100) DEFAULT NULL,
  `filesize` int(20) NOT NULL DEFAULT '0',
  `filetime` int(11) NOT NULL DEFAULT '0',
  `thumbnail` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_attach_quota`
--

CREATE TABLE `phpbb_attach_quota` (
  `user_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `group_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `quota_type` smallint(2) NOT NULL DEFAULT '0',
  `quota_limit_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_auth_access`
--

CREATE TABLE `phpbb_auth_access` (
  `group_id` mediumint(8) NOT NULL DEFAULT '0',
  `forum_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `auth_view` tinyint(1) NOT NULL DEFAULT '0',
  `auth_read` tinyint(1) NOT NULL DEFAULT '0',
  `auth_post` tinyint(1) NOT NULL DEFAULT '0',
  `auth_reply` tinyint(1) NOT NULL DEFAULT '0',
  `auth_edit` tinyint(1) NOT NULL DEFAULT '0',
  `auth_delete` tinyint(1) NOT NULL DEFAULT '0',
  `auth_sticky` tinyint(1) NOT NULL DEFAULT '0',
  `auth_announce` tinyint(1) NOT NULL DEFAULT '0',
  `auth_globalannounce` tinyint(1) NOT NULL DEFAULT '0',
  `auth_vote` tinyint(1) NOT NULL DEFAULT '0',
  `auth_pollcreate` tinyint(1) NOT NULL DEFAULT '0',
  `auth_attachments` tinyint(1) NOT NULL DEFAULT '0',
  `auth_mod` tinyint(1) NOT NULL DEFAULT '0',
  `auth_download` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_banlist`
--

CREATE TABLE `phpbb_banlist` (
  `ban_id` mediumint(8) UNSIGNED NOT NULL,
  `ban_userid` mediumint(8) NOT NULL DEFAULT '0',
  `ban_ip` char(8) NOT NULL DEFAULT '',
  `ban_email` varchar(255) DEFAULT NULL,
  `ban_time` int(11) DEFAULT NULL,
  `ban_expire_time` int(11) DEFAULT NULL,
  `ban_by_userid` mediumint(8) DEFAULT NULL,
  `ban_priv_reason` text,
  `ban_pub_reason_mode` tinyint(1) DEFAULT NULL,
  `ban_pub_reason` text,
  `ban_host` varchar(255) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_birthday`
--

CREATE TABLE `phpbb_birthday` (
  `user_id` mediumint(9) NOT NULL DEFAULT '0',
  `send_user_id` mediumint(9) NOT NULL DEFAULT '0',
  `send_year` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_categories`
--

CREATE TABLE `phpbb_categories` (
  `cat_id` mediumint(8) UNSIGNED NOT NULL,
  `cat_title` varchar(254) DEFAULT NULL,
  `cat_order` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `cat_main_type` char(1) DEFAULT NULL,
  `cat_main` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `cat_desc` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_config`
--

CREATE TABLE `phpbb_config` (
  `config_name` varchar(255) NOT NULL DEFAULT '',
  `config_value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_config_saved`
--

CREATE TABLE `phpbb_config_saved` (
  `config_date` int(11) NOT NULL DEFAULT '0',
  `config_value` longtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_disallow`
--

CREATE TABLE `phpbb_disallow` (
  `disallow_id` mediumint(8) UNSIGNED NOT NULL,
  `disallow_username` varchar(25) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_extensions`
--

CREATE TABLE `phpbb_extensions` (
  `ext_id` mediumint(8) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `extension` varchar(100) NOT NULL DEFAULT '',
  `comment` varchar(100) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_extension_groups`
--

CREATE TABLE `phpbb_extension_groups` (
  `group_id` mediumint(8) NOT NULL,
  `group_name` char(20) NOT NULL DEFAULT '',
  `cat_id` tinyint(2) NOT NULL DEFAULT '0',
  `allow_group` tinyint(1) NOT NULL DEFAULT '0',
  `download_mode` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `upload_icon` varchar(100) DEFAULT '',
  `max_filesize` int(20) NOT NULL DEFAULT '0',
  `forum_permissions` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_forbidden_extensions`
--

CREATE TABLE `phpbb_forbidden_extensions` (
  `ext_id` mediumint(8) UNSIGNED NOT NULL,
  `extension` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_forums`
--

CREATE TABLE `phpbb_forums` (
  `forum_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `cat_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `forum_name` varchar(254) DEFAULT '',
  `forum_desc` text,
  `forum_status` tinyint(1) NOT NULL DEFAULT '0',
  `forum_order` mediumint(8) UNSIGNED NOT NULL DEFAULT '1',
  `forum_posts` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `forum_topics` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `forum_last_post_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `prune_next` int(11) DEFAULT NULL,
  `prune_enable` tinyint(1) NOT NULL DEFAULT '0',
  `auth_view` tinyint(2) NOT NULL DEFAULT '0',
  `auth_read` tinyint(2) NOT NULL DEFAULT '0',
  `auth_post` tinyint(2) NOT NULL DEFAULT '0',
  `auth_reply` tinyint(2) NOT NULL DEFAULT '0',
  `auth_edit` tinyint(2) NOT NULL DEFAULT '0',
  `auth_delete` tinyint(2) NOT NULL DEFAULT '0',
  `auth_sticky` tinyint(2) NOT NULL DEFAULT '0',
  `auth_announce` tinyint(2) NOT NULL DEFAULT '0',
  `auth_globalannounce` tinyint(2) NOT NULL DEFAULT '3',
  `auth_vote` tinyint(2) NOT NULL DEFAULT '0',
  `auth_pollcreate` tinyint(2) NOT NULL DEFAULT '0',
  `auth_attachments` tinyint(2) NOT NULL DEFAULT '0',
  `auth_download` tinyint(2) NOT NULL DEFAULT '0',
  `password` varchar(20) NOT NULL DEFAULT '',
  `forum_sort` varchar(12) NOT NULL,
  `forum_color` varchar(6) NOT NULL DEFAULT '',
  `forum_link` varchar(255) DEFAULT NULL,
  `forum_link_internal` tinyint(1) NOT NULL DEFAULT '0',
  `forum_link_hit_count` tinyint(1) NOT NULL DEFAULT '0',
  `forum_link_hit` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `main_type` char(1) DEFAULT NULL,
  `forum_moderate` tinyint(1) NOT NULL DEFAULT '0',
  `no_count` tinyint(1) NOT NULL DEFAULT '0',
  `forum_trash` smallint(1) NOT NULL DEFAULT '0',
  `forum_separate` smallint(1) NOT NULL DEFAULT '2',
  `forum_show_ga` smallint(1) NOT NULL DEFAULT '1',
  `forum_tree_grade` tinyint(1) NOT NULL DEFAULT '3',
  `forum_tree_req` tinyint(1) NOT NULL DEFAULT '0',
  `forum_no_split` tinyint(1) NOT NULL DEFAULT '0',
  `forum_no_helped` tinyint(1) NOT NULL DEFAULT '0',
  `topic_tags` varchar(255) DEFAULT '',
  `locked_bottom` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_forum_prune`
--

CREATE TABLE `phpbb_forum_prune` (
  `prune_id` mediumint(8) UNSIGNED NOT NULL,
  `forum_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `prune_days` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `prune_freq` smallint(5) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_groups`
--

CREATE TABLE `phpbb_groups` (
  `group_id` mediumint(8) NOT NULL,
  `group_type` tinyint(4) NOT NULL DEFAULT '1',
  `group_name` varchar(120) NOT NULL DEFAULT '',
  `group_description` varchar(255) NOT NULL DEFAULT '',
  `group_moderator` mediumint(8) NOT NULL DEFAULT '0',
  `group_single_user` tinyint(1) NOT NULL DEFAULT '1',
  `group_order` mediumint(8) NOT NULL DEFAULT '0',
  `group_count` int(4) UNSIGNED DEFAULT '99999999',
  `group_count_enable` smallint(2) UNSIGNED DEFAULT '0',
  `group_mail_enable` smallint(1) DEFAULT '0',
  `group_no_unsub` smallint(1) DEFAULT '0',
  `group_color` varchar(6) DEFAULT NULL,
  `group_prefix` varchar(8) DEFAULT NULL,
  `group_style` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_ignores`
--

CREATE TABLE `phpbb_ignores` (
  `user_id` mediumint(8) NOT NULL DEFAULT '0',
  `user_ignore` mediumint(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_jr_admin_users`
--

CREATE TABLE `phpbb_jr_admin_users` (
  `user_id` mediumint(9) NOT NULL DEFAULT '0',
  `user_jr_admin` varchar(254) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_logs`
--

CREATE TABLE `phpbb_logs` (
  `id_log` mediumint(10) NOT NULL,
  `mode` varchar(50) DEFAULT '',
  `topic_id` mediumint(10) DEFAULT '0',
  `user_id` mediumint(8) DEFAULT '0',
  `username` varchar(25) DEFAULT '',
  `user_ip` char(8) NOT NULL DEFAULT '0',
  `time` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_mass_email`
--

CREATE TABLE `phpbb_mass_email` (
  `mass_email_user_id` mediumint(8) NOT NULL DEFAULT '0',
  `mass_email_text` longtext,
  `mass_email_subject` text,
  `mass_email_bcc` longtext,
  `mass_email_html` tinyint(1) NOT NULL DEFAULT '0',
  `mass_email_to` varchar(128) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_pa_cat`
--

CREATE TABLE `phpbb_pa_cat` (
  `cat_id` int(10) NOT NULL,
  `cat_name` text,
  `cat_desc` text,
  `cat_files` int(10) DEFAULT NULL,
  `cat_1xid` int(10) DEFAULT NULL,
  `cat_parent` int(50) DEFAULT NULL,
  `cat_order` int(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_pa_comments`
--

CREATE TABLE `phpbb_pa_comments` (
  `comments_id` int(10) NOT NULL,
  `file_id` int(10) NOT NULL DEFAULT '0',
  `comments_text` text NOT NULL,
  `comments_title` text NOT NULL,
  `comments_time` int(50) NOT NULL DEFAULT '0',
  `comment_bbcode_uid` varchar(10) DEFAULT NULL,
  `poster_id` mediumint(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_pa_custom`
--

CREATE TABLE `phpbb_pa_custom` (
  `custom_id` int(50) NOT NULL,
  `custom_name` text NOT NULL,
  `custom_description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_pa_customdata`
--

CREATE TABLE `phpbb_pa_customdata` (
  `customdata_file` int(50) NOT NULL DEFAULT '0',
  `customdata_custom` int(50) NOT NULL DEFAULT '0',
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_pa_files`
--

CREATE TABLE `phpbb_pa_files` (
  `file_id` int(10) NOT NULL,
  `file_name` text,
  `file_desc` text,
  `file_creator` text,
  `file_version` text,
  `file_longdesc` text,
  `file_ssurl` text,
  `file_dlurl` text,
  `file_time` int(50) DEFAULT NULL,
  `file_catid` int(10) DEFAULT NULL,
  `file_posticon` text,
  `file_license` int(10) DEFAULT NULL,
  `file_dls` int(10) DEFAULT NULL,
  `file_last` int(50) DEFAULT NULL,
  `file_pin` int(2) DEFAULT NULL,
  `file_docsurl` text,
  `file_rating` double(6,4) NOT NULL DEFAULT '0.0000',
  `file_totalvotes` int(255) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_pa_license`
--

CREATE TABLE `phpbb_pa_license` (
  `license_id` int(10) NOT NULL,
  `license_name` text,
  `license_text` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_pa_settings`
--

CREATE TABLE `phpbb_pa_settings` (
  `settings_id` int(1) NOT NULL DEFAULT '1',
  `settings_dbname` text NOT NULL,
  `settings_sitename` text NOT NULL,
  `settings_dbdescription` text NOT NULL,
  `settings_dburl` text NOT NULL,
  `settings_topnumber` int(5) NOT NULL DEFAULT '0',
  `settings_homeurl` text NOT NULL,
  `settings_newdays` int(5) NOT NULL DEFAULT '0',
  `settings_stats` int(5) NOT NULL DEFAULT '0',
  `settings_viewall` int(5) NOT NULL DEFAULT '0',
  `settings_showss` int(5) NOT NULL DEFAULT '0',
  `settings_disable` int(5) NOT NULL DEFAULT '0',
  `allow_html` int(5) NOT NULL DEFAULT '0',
  `allow_bbcode` int(5) NOT NULL DEFAULT '0',
  `allow_smilies` int(5) NOT NULL DEFAULT '0',
  `allow_comment_links` int(5) NOT NULL DEFAULT '0',
  `no_comment_link_message` varchar(255) NOT NULL DEFAULT '',
  `allow_comment_images` int(5) NOT NULL DEFAULT '0',
  `no_comment_image_message` varchar(255) NOT NULL DEFAULT '',
  `max_comment_chars` int(255) NOT NULL DEFAULT '0',
  `directly_linked` int(5) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_pa_votes`
--

CREATE TABLE `phpbb_pa_votes` (
  `votes_ip` varchar(50) NOT NULL DEFAULT '0',
  `votes_file` int(50) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_portal_config`
--

CREATE TABLE `phpbb_portal_config` (
  `config_name` varchar(255) NOT NULL DEFAULT '',
  `config_value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_posts`
--

CREATE TABLE `phpbb_posts` (
  `post_id` mediumint(8) UNSIGNED NOT NULL,
  `topic_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `forum_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `poster_id` mediumint(8) NOT NULL DEFAULT '0',
  `post_time` int(11) NOT NULL DEFAULT '0',
  `post_start_time` int(11) NOT NULL DEFAULT '0',
  `poster_ip` char(8) NOT NULL DEFAULT '',
  `post_username` varchar(25) NOT NULL DEFAULT '',
  `enable_bbcode` tinyint(1) NOT NULL DEFAULT '1',
  `enable_html` tinyint(1) NOT NULL DEFAULT '0',
  `enable_smilies` tinyint(1) NOT NULL DEFAULT '1',
  `enable_sig` tinyint(1) NOT NULL DEFAULT '1',
  `post_edit_time` int(11) DEFAULT '0',
  `post_edit_count` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `post_attachment` tinyint(1) NOT NULL DEFAULT '0',
  `user_agent` varchar(255) NOT NULL DEFAULT '',
  `post_icon` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `post_expire` int(11) NOT NULL DEFAULT '0',
  `reporter_id` mediumint(8) NOT NULL DEFAULT '0',
  `post_marked` enum('n','y') DEFAULT NULL,
  `post_approve` tinyint(1) NOT NULL DEFAULT '1',
  `poster_delete` tinyint(1) DEFAULT '0',
  `post_edit_by` mediumint(8) NOT NULL DEFAULT '0',
  `post_parent` mediumint(8) NOT NULL DEFAULT '0',
  `post_order` mediumint(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_posts_text`
--

CREATE TABLE `phpbb_posts_text` (
  `post_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `bbcode_uid` char(10) NOT NULL DEFAULT '',
  `post_subject` char(60) NOT NULL DEFAULT '',
  `post_text` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_posts_text_history`
--

CREATE TABLE `phpbb_posts_text_history` (
  `th_id` mediumint(9) NOT NULL,
  `th_post_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `th_post_text` text,
  `th_user_id` mediumint(8) NOT NULL DEFAULT '0',
  `th_time` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_privmsgs`
--

CREATE TABLE `phpbb_privmsgs` (
  `privmsgs_id` mediumint(8) UNSIGNED NOT NULL,
  `privmsgs_type` tinyint(4) NOT NULL DEFAULT '0',
  `privmsgs_subject` varchar(255) NOT NULL DEFAULT '0',
  `privmsgs_from_userid` mediumint(8) NOT NULL DEFAULT '0',
  `privmsgs_to_userid` mediumint(8) NOT NULL DEFAULT '0',
  `privmsgs_date` int(11) NOT NULL DEFAULT '0',
  `privmsgs_ip` char(8) NOT NULL DEFAULT '',
  `privmsgs_enable_bbcode` tinyint(1) NOT NULL DEFAULT '1',
  `privmsgs_enable_html` tinyint(1) NOT NULL DEFAULT '0',
  `privmsgs_enable_smilies` tinyint(1) NOT NULL DEFAULT '1',
  `privmsgs_attach_sig` tinyint(1) NOT NULL DEFAULT '1',
  `privmsgs_attachment` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_privmsgs_text`
--

CREATE TABLE `phpbb_privmsgs_text` (
  `privmsgs_text_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `privmsgs_bbcode_uid` char(10) NOT NULL DEFAULT '0',
  `privmsgs_text` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_profile_fields`
--

CREATE TABLE `phpbb_profile_fields` (
  `id` tinyint(1) NOT NULL,
  `desc_short` varchar(255) NOT NULL,
  `desc_long` varchar(255) DEFAULT NULL,
  `makelinks` tinyint(1) NOT NULL DEFAULT '0',
  `max_value` int(8) NOT NULL DEFAULT '45',
  `min_value` int(8) NOT NULL DEFAULT '1',
  `numerics` tinyint(1) NOT NULL DEFAULT '0',
  `jumpbox` text,
  `requires` tinyint(1) NOT NULL DEFAULT '0',
  `view_post` tinyint(1) NOT NULL DEFAULT '2',
  `view_profile` tinyint(1) NOT NULL DEFAULT '1',
  `set_form` tinyint(1) NOT NULL DEFAULT '0',
  `no_forum` varchar(255) NOT NULL DEFAULT '',
  `prefix` varchar(255) NOT NULL DEFAULT '',
  `suffix` varchar(255) NOT NULL DEFAULT '',
  `editable` tinyint(1) NOT NULL DEFAULT '1',
  `view_by` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_quota_limits`
--

CREATE TABLE `phpbb_quota_limits` (
  `quota_limit_id` mediumint(8) UNSIGNED NOT NULL,
  `quota_desc` varchar(20) NOT NULL DEFAULT '',
  `quota_limit` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_ranks`
--

CREATE TABLE `phpbb_ranks` (
  `rank_id` smallint(5) UNSIGNED NOT NULL,
  `rank_title` varchar(50) NOT NULL DEFAULT '',
  `rank_min` mediumint(8) NOT NULL DEFAULT '0',
  `rank_special` tinyint(1) DEFAULT '0',
  `rank_image` varchar(255) DEFAULT '',
  `rank_group` mediumint(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_read_history`
--

CREATE TABLE `phpbb_read_history` (
  `user_id` mediumint(8) NOT NULL DEFAULT '0',
  `post_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `topic_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `forum_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_search_results`
--

CREATE TABLE `phpbb_search_results` (
  `search_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `session_id` char(32) NOT NULL DEFAULT '',
  `search_array` text NOT NULL,
  `search_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_search_wordlist`
--

CREATE TABLE `phpbb_search_wordlist` (
  `word_text` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `word_id` mediumint(8) UNSIGNED NOT NULL,
  `word_common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_search_wordmatch`
--

CREATE TABLE `phpbb_search_wordmatch` (
  `post_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `word_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `title_match` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_sessions`
--

CREATE TABLE `phpbb_sessions` (
  `session_id` char(32) NOT NULL DEFAULT '',
  `session_user_id` mediumint(8) NOT NULL DEFAULT '0',
  `session_start` int(11) NOT NULL DEFAULT '0',
  `session_time` int(11) NOT NULL DEFAULT '0',
  `session_ip` char(8) NOT NULL DEFAULT '0',
  `session_page` int(11) NOT NULL DEFAULT '0',
  `session_logged_in` tinyint(1) NOT NULL DEFAULT '0',
  `session_admin` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_sessions_keys`
--

CREATE TABLE `phpbb_sessions_keys` (
  `key_id` varchar(32) NOT NULL DEFAULT '0',
  `user_id` mediumint(8) NOT NULL DEFAULT '0',
  `last_ip` varchar(8) NOT NULL DEFAULT '0',
  `last_login` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_shoutbox`
--

CREATE TABLE `phpbb_shoutbox` (
  `id` int(11) NOT NULL,
  `sb_user_id` int(11) NOT NULL DEFAULT '0',
  `msg` text NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_shoutbox_config`
--

CREATE TABLE `phpbb_shoutbox_config` (
  `config_name` varchar(255) NOT NULL DEFAULT '',
  `config_value` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_smilies`
--

CREATE TABLE `phpbb_smilies` (
  `smilies_id` smallint(5) UNSIGNED NOT NULL,
  `code` varchar(50) DEFAULT '',
  `smile_url` varchar(100) DEFAULT '',
  `emoticon` varchar(75) DEFAULT '',
  `smile_order` mediumint(8) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_stats_config`
--

CREATE TABLE `phpbb_stats_config` (
  `config_name` varchar(50) NOT NULL DEFAULT '',
  `config_value` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_stats_modules`
--

CREATE TABLE `phpbb_stats_modules` (
  `module_id` tinyint(8) NOT NULL DEFAULT '0',
  `name` varchar(150) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `installed` tinyint(1) NOT NULL DEFAULT '0',
  `display_order` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `update_time` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `auth_value` tinyint(2) NOT NULL DEFAULT '0',
  `module_info_cache` blob,
  `module_db_cache` blob,
  `module_result_cache` blob,
  `module_info_time` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `module_cache_time` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_themes`
--

CREATE TABLE `phpbb_themes` (
  `themes_id` mediumint(8) UNSIGNED NOT NULL,
  `template_name` varchar(30) NOT NULL DEFAULT '',
  `style_name` varchar(30) NOT NULL DEFAULT '',
  `head_stylesheet` varchar(100) DEFAULT NULL,
  `body_background` varchar(100) DEFAULT NULL,
  `body_bgcolor` varchar(6) DEFAULT NULL,
  `body_text` varchar(6) DEFAULT NULL,
  `body_link` varchar(6) DEFAULT NULL,
  `body_vlink` varchar(6) DEFAULT NULL,
  `body_alink` varchar(6) DEFAULT NULL,
  `body_hlink` varchar(6) DEFAULT NULL,
  `tr_color1` varchar(6) DEFAULT NULL,
  `tr_color2` varchar(6) DEFAULT NULL,
  `tr_color3` varchar(6) DEFAULT NULL,
  `tr_color_helped` varchar(6) DEFAULT NULL,
  `tr_class1` varchar(25) DEFAULT NULL,
  `tr_class2` varchar(25) DEFAULT NULL,
  `tr_class3` varchar(25) DEFAULT NULL,
  `th_color1` varchar(6) DEFAULT NULL,
  `th_color2` varchar(6) DEFAULT NULL,
  `th_color3` varchar(6) DEFAULT NULL,
  `th_class1` varchar(25) DEFAULT NULL,
  `th_class2` varchar(25) DEFAULT NULL,
  `th_class3` varchar(25) DEFAULT NULL,
  `td_color1` varchar(6) DEFAULT NULL,
  `td_color2` varchar(6) DEFAULT NULL,
  `td_color3` varchar(6) DEFAULT NULL,
  `td_class1` varchar(25) DEFAULT NULL,
  `td_class2` varchar(25) DEFAULT NULL,
  `td_class3` varchar(25) DEFAULT NULL,
  `fontface1` varchar(50) DEFAULT NULL,
  `fontface2` varchar(50) DEFAULT NULL,
  `fontface3` varchar(50) DEFAULT NULL,
  `fontsize1` tinyint(4) DEFAULT NULL,
  `fontsize2` tinyint(4) DEFAULT NULL,
  `fontsize3` tinyint(4) DEFAULT NULL,
  `fontcolor1` varchar(6) DEFAULT NULL,
  `fontcolor2` varchar(6) DEFAULT NULL,
  `fontcolor3` varchar(6) DEFAULT NULL,
  `fontcolor_admin` varchar(6) DEFAULT NULL,
  `fontcolor_jradmin` varchar(6) DEFAULT NULL,
  `fontcolor_mod` varchar(6) DEFAULT NULL,
  `factive_color` varchar(6) DEFAULT NULL,
  `faonmouse_color` varchar(6) DEFAULT NULL,
  `faonmouse2_color` varchar(6) DEFAULT NULL,
  `span_class1` varchar(25) DEFAULT NULL,
  `span_class2` varchar(25) DEFAULT NULL,
  `span_class3` varchar(25) DEFAULT NULL,
  `img_size_poll` smallint(5) UNSIGNED DEFAULT NULL,
  `img_size_privmsg` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_themes_name`
--

CREATE TABLE `phpbb_themes_name` (
  `themes_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `tr_color1_name` char(50) DEFAULT NULL,
  `tr_color2_name` char(50) DEFAULT NULL,
  `tr_color3_name` char(50) DEFAULT NULL,
  `tr_class1_name` char(50) DEFAULT NULL,
  `tr_class2_name` char(50) DEFAULT NULL,
  `tr_class3_name` char(50) DEFAULT NULL,
  `th_color1_name` char(50) DEFAULT NULL,
  `th_color2_name` char(50) DEFAULT NULL,
  `th_color3_name` char(50) DEFAULT NULL,
  `th_class1_name` char(50) DEFAULT NULL,
  `th_class2_name` char(50) DEFAULT NULL,
  `th_class3_name` char(50) DEFAULT NULL,
  `td_color1_name` char(50) DEFAULT NULL,
  `td_color2_name` char(50) DEFAULT NULL,
  `td_color3_name` char(50) DEFAULT NULL,
  `td_class1_name` char(50) DEFAULT NULL,
  `td_class2_name` char(50) DEFAULT NULL,
  `td_class3_name` char(50) DEFAULT NULL,
  `fontface1_name` char(50) DEFAULT NULL,
  `fontface2_name` char(50) DEFAULT NULL,
  `fontface3_name` char(50) DEFAULT NULL,
  `fontsize1_name` char(50) DEFAULT NULL,
  `fontsize2_name` char(50) DEFAULT NULL,
  `fontsize3_name` char(50) DEFAULT NULL,
  `fontcolor1_name` char(50) DEFAULT NULL,
  `fontcolor2_name` char(50) DEFAULT NULL,
  `fontcolor3_name` char(50) DEFAULT NULL,
  `span_class1_name` char(50) DEFAULT NULL,
  `span_class2_name` char(50) DEFAULT NULL,
  `span_class3_name` char(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_topics`
--

CREATE TABLE `phpbb_topics` (
  `topic_id` mediumint(8) UNSIGNED NOT NULL,
  `forum_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `topic_title` char(60) NOT NULL DEFAULT '',
  `topic_poster` mediumint(8) NOT NULL DEFAULT '0',
  `topic_time` int(11) NOT NULL DEFAULT '0',
  `topic_views` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `topic_replies` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `topic_status` tinyint(1) NOT NULL DEFAULT '0',
  `topic_vote` tinyint(1) NOT NULL DEFAULT '0',
  `topic_type` tinyint(1) NOT NULL DEFAULT '0',
  `topic_first_post_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `topic_last_post_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `topic_moved_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `topic_attachment` tinyint(1) NOT NULL DEFAULT '0',
  `topic_icon` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `topic_expire` int(11) NOT NULL DEFAULT '0',
  `topic_color` varchar(8) DEFAULT NULL,
  `topic_title_e` char(100) NOT NULL DEFAULT '',
  `topic_action` tinyint(1) DEFAULT '0',
  `topic_action_user` mediumint(8) NOT NULL DEFAULT '0',
  `topic_action_date` int(11) NOT NULL DEFAULT '0',
  `topic_tree_width` smallint(2) NOT NULL DEFAULT '0',
  `topic_accept` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_topics_ignore`
--

CREATE TABLE `phpbb_topics_ignore` (
  `topic_id` mediumint(8) UNSIGNED NOT NULL,
  `user_id` mediumint(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_topics_watch`
--

CREATE TABLE `phpbb_topics_watch` (
  `topic_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` mediumint(8) NOT NULL DEFAULT '0',
  `notify_status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_topic_view`
--

CREATE TABLE `phpbb_topic_view` (
  `topic_id` mediumint(8) NOT NULL DEFAULT '0',
  `user_id` mediumint(8) NOT NULL DEFAULT '0',
  `view_time` int(11) NOT NULL DEFAULT '0',
  `view_count` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_users`
--

CREATE TABLE `phpbb_users` (
  `user_id` mediumint(8) NOT NULL,
  `user_active` tinyint(1) DEFAULT '1',
  `username` varchar(25) NOT NULL DEFAULT '',
  `user_password` varchar(40) NOT NULL DEFAULT '',
  `user_session_time` int(11) NOT NULL DEFAULT '0',
  `user_session_page` smallint(5) NOT NULL DEFAULT '0',
  `user_lastvisit` int(11) NOT NULL DEFAULT '0',
  `user_regdate` int(11) NOT NULL DEFAULT '0',
  `user_level` tinyint(1) DEFAULT '0',
  `user_posts` mediumint(6) UNSIGNED NOT NULL DEFAULT '0',
  `user_timezone` decimal(5,2) NOT NULL DEFAULT '0.00',
  `user_style` tinyint(2) DEFAULT '1',
  `user_lang` varchar(12) NOT NULL DEFAULT '',
  `user_new_privmsg` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `user_unread_privmsg` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `user_last_privmsg` int(11) NOT NULL DEFAULT '0',
  `user_emailtime` int(11) DEFAULT '0',
  `user_viewemail` tinyint(1) DEFAULT '1',
  `user_viewaim` tinyint(1) DEFAULT '1',
  `user_attachsig` tinyint(1) DEFAULT '1',
  `user_allowhtml` tinyint(1) DEFAULT '1',
  `user_allowbbcode` tinyint(1) DEFAULT '1',
  `user_allowsmile` tinyint(1) DEFAULT '1',
  `user_allowavatar` tinyint(1) NOT NULL DEFAULT '1',
  `user_allowsig` tinyint(1) NOT NULL DEFAULT '1',
  `user_allow_pm` tinyint(1) NOT NULL DEFAULT '1',
  `user_allow_viewonline` tinyint(1) NOT NULL DEFAULT '1',
  `user_notify` tinyint(1) NOT NULL DEFAULT '1',
  `user_notify_pm` tinyint(1) NOT NULL DEFAULT '1',
  `user_popup_pm` tinyint(1) NOT NULL DEFAULT '0',
  `user_rank` int(11) DEFAULT '0',
  `user_avatar` varchar(100) DEFAULT '',
  `user_avatar_type` tinyint(1) NOT NULL DEFAULT '0',
  `user_email` varchar(255) DEFAULT '',
  `user_icq` varchar(15) DEFAULT '',
  `user_website` varchar(255) DEFAULT '',
  `user_from` varchar(64) DEFAULT '',
  `user_field_9` int(2) NOT NULL DEFAULT '0',
  `user_allow_field_9` tinyint(1) DEFAULT '1',
  `user_field_7` varchar(45) DEFAULT '',
  `user_allow_field_7` tinyint(1) DEFAULT '1',
  `user_field_2` varchar(45) DEFAULT '',
  `user_allow_field_2` tinyint(1) DEFAULT '1',
  `user_sig` text,
  `user_sig_bbcode_uid` char(10) DEFAULT '',
  `user_sig_image` varchar(100) NOT NULL DEFAULT '',
  `user_aim` varchar(255) DEFAULT '',
  `user_yim` varchar(255) DEFAULT '',
  `user_msnm` varchar(255) DEFAULT '',
  `user_occ` varchar(100) DEFAULT '',
  `user_interests` varchar(255) DEFAULT '',
  `user_actkey` varchar(32) DEFAULT '',
  `user_newpasswd` varchar(40) DEFAULT '',
  `user_birthday` int(6) NOT NULL DEFAULT '999999',
  `user_next_birthday_greeting` int(4) NOT NULL DEFAULT '0',
  `user_custom_rank` varchar(100) DEFAULT '',
  `user_photo` varchar(100) DEFAULT '',
  `user_photo_type` tinyint(1) NOT NULL DEFAULT '0',
  `user_custom_color` varchar(6) DEFAULT '',
  `user_badlogin` smallint(2) NOT NULL DEFAULT '0',
  `user_blocktime` int(11) NOT NULL DEFAULT '0',
  `user_block_by` char(8) DEFAULT '',
  `disallow_forums` varchar(254) DEFAULT '',
  `can_custom_ranks` tinyint(1) NOT NULL DEFAULT '1',
  `can_custom_color` tinyint(1) NOT NULL DEFAULT '1',
  `user_gender` tinyint(1) NOT NULL DEFAULT '0',
  `can_topic_color` tinyint(1) NOT NULL DEFAULT '1',
  `allowpm` tinyint(1) DEFAULT '1',
  `no_report_popup` tinyint(1) NOT NULL DEFAULT '0',
  `refresh_report_popup` tinyint(1) NOT NULL DEFAULT '0',
  `no_report_mail` tinyint(1) NOT NULL DEFAULT '0',
  `user_avatar_width` smallint(3) DEFAULT NULL,
  `user_avatar_height` smallint(3) DEFAULT NULL,
  `special_rank` mediumint(8) UNSIGNED DEFAULT NULL,
  `user_allow_helped` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `user_ip` char(8) DEFAULT NULL,
  `user_ip_login_check` tinyint(1) NOT NULL DEFAULT '1',
  `user_spend_time` int(8) NOT NULL DEFAULT '0',
  `user_visit` int(7) NOT NULL DEFAULT '0',
  `user_session_start` int(11) NOT NULL DEFAULT '0',
  `read_tracking_last_update` int(11) NOT NULL DEFAULT '0',
  `user_jr` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_users_warnings`
--

CREATE TABLE `phpbb_users_warnings` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `userid` mediumint(8) NOT NULL DEFAULT '0',
  `modid` mediumint(8) NOT NULL DEFAULT '0',
  `date` int(11) NOT NULL DEFAULT '0',
  `value` mediumint(8) NOT NULL DEFAULT '0',
  `reason` text,
  `archive` tinyint(1) NOT NULL DEFAULT '0',
  `warning_viewed` smallint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_user_group`
--

CREATE TABLE `phpbb_user_group` (
  `group_id` mediumint(8) NOT NULL DEFAULT '0',
  `user_id` mediumint(8) NOT NULL DEFAULT '0',
  `user_pending` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_vote_desc`
--

CREATE TABLE `phpbb_vote_desc` (
  `vote_id` mediumint(8) UNSIGNED NOT NULL,
  `topic_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `vote_text` text NOT NULL,
  `vote_start` int(11) NOT NULL DEFAULT '0',
  `vote_length` int(11) NOT NULL DEFAULT '0',
  `vote_max` int(3) NOT NULL DEFAULT '1',
  `vote_voted` int(7) NOT NULL DEFAULT '0',
  `vote_hide` tinyint(1) NOT NULL DEFAULT '0',
  `vote_tothide` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_vote_results`
--

CREATE TABLE `phpbb_vote_results` (
  `vote_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `vote_option_id` tinyint(4) UNSIGNED NOT NULL DEFAULT '0',
  `vote_option_text` varchar(255) NOT NULL DEFAULT '',
  `vote_result` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_vote_voters`
--

CREATE TABLE `phpbb_vote_voters` (
  `vote_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `vote_user_id` mediumint(8) NOT NULL DEFAULT '0',
  `vote_user_ip` char(8) NOT NULL DEFAULT '',
  `vote_cast` tinyint(4) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `phpbb_words`
--

CREATE TABLE `phpbb_words` (
  `word_id` mediumint(8) UNSIGNED NOT NULL,
  `word` char(100) NOT NULL DEFAULT '',
  `replacement` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `phpbb_advertisement`
--
ALTER TABLE `phpbb_advertisement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phpbb_adv_person`
--
ALTER TABLE `phpbb_adv_person`
  ADD PRIMARY KEY (`user_id`,`person_id`);

--
-- Indexes for table `phpbb_anti_robotic_reg`
--
ALTER TABLE `phpbb_anti_robotic_reg`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `phpbb_attachments`
--
ALTER TABLE `phpbb_attachments`
  ADD KEY `attach_id_post_id` (`attach_id`,`post_id`),
  ADD KEY `attach_id_privmsgs_id` (`attach_id`,`privmsgs_id`),
  ADD KEY `user_id_1` (`user_id_1`),
  ADD KEY `user_id_2` (`user_id_2`);

--
-- Indexes for table `phpbb_attachments_config`
--
ALTER TABLE `phpbb_attachments_config`
  ADD PRIMARY KEY (`config_name`);

--
-- Indexes for table `phpbb_attachments_desc`
--
ALTER TABLE `phpbb_attachments_desc`
  ADD PRIMARY KEY (`attach_id`),
  ADD KEY `filetime` (`filetime`),
  ADD KEY `physical_filename` (`physical_filename`(10)),
  ADD KEY `filesize` (`filesize`);

--
-- Indexes for table `phpbb_attach_quota`
--
ALTER TABLE `phpbb_attach_quota`
  ADD KEY `quota_type` (`quota_type`);

--
-- Indexes for table `phpbb_auth_access`
--
ALTER TABLE `phpbb_auth_access`
  ADD KEY `group_id` (`group_id`),
  ADD KEY `forum_id` (`forum_id`);

--
-- Indexes for table `phpbb_banlist`
--
ALTER TABLE `phpbb_banlist`
  ADD PRIMARY KEY (`ban_id`),
  ADD KEY `ban_ip_user_id` (`ban_ip`,`ban_userid`);

--
-- Indexes for table `phpbb_birthday`
--
ALTER TABLE `phpbb_birthday`
  ADD PRIMARY KEY (`user_id`,`send_user_id`,`send_year`);

--
-- Indexes for table `phpbb_categories`
--
ALTER TABLE `phpbb_categories`
  ADD PRIMARY KEY (`cat_id`),
  ADD KEY `cat_order` (`cat_order`);

--
-- Indexes for table `phpbb_config`
--
ALTER TABLE `phpbb_config`
  ADD PRIMARY KEY (`config_name`);

--
-- Indexes for table `phpbb_config_saved`
--
ALTER TABLE `phpbb_config_saved`
  ADD PRIMARY KEY (`config_date`);

--
-- Indexes for table `phpbb_disallow`
--
ALTER TABLE `phpbb_disallow`
  ADD PRIMARY KEY (`disallow_id`);

--
-- Indexes for table `phpbb_extensions`
--
ALTER TABLE `phpbb_extensions`
  ADD PRIMARY KEY (`ext_id`);

--
-- Indexes for table `phpbb_extension_groups`
--
ALTER TABLE `phpbb_extension_groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `phpbb_forbidden_extensions`
--
ALTER TABLE `phpbb_forbidden_extensions`
  ADD PRIMARY KEY (`ext_id`);

--
-- Indexes for table `phpbb_forums`
--
ALTER TABLE `phpbb_forums`
  ADD PRIMARY KEY (`forum_id`),
  ADD KEY `forums_order` (`forum_order`),
  ADD KEY `cat_id` (`cat_id`),
  ADD KEY `forum_last_post_id` (`forum_last_post_id`),
  ADD KEY `no_count` (`no_count`);

--
-- Indexes for table `phpbb_forum_prune`
--
ALTER TABLE `phpbb_forum_prune`
  ADD PRIMARY KEY (`prune_id`),
  ADD KEY `forum_id` (`forum_id`);

--
-- Indexes for table `phpbb_groups`
--
ALTER TABLE `phpbb_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `group_single_user` (`group_single_user`),
  ADD KEY `group_type` (`group_type`);

--
-- Indexes for table `phpbb_ignores`
--
ALTER TABLE `phpbb_ignores`
  ADD PRIMARY KEY (`user_id`,`user_ignore`);

--
-- Indexes for table `phpbb_jr_admin_users`
--
ALTER TABLE `phpbb_jr_admin_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `phpbb_logs`
--
ALTER TABLE `phpbb_logs`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `phpbb_mass_email`
--
ALTER TABLE `phpbb_mass_email`
  ADD PRIMARY KEY (`mass_email_user_id`);

--
-- Indexes for table `phpbb_pa_cat`
--
ALTER TABLE `phpbb_pa_cat`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `phpbb_pa_comments`
--
ALTER TABLE `phpbb_pa_comments`
  ADD PRIMARY KEY (`comments_id`),
  ADD KEY `comments_id` (`comments_id`),
  ADD KEY `comment_bbcode_uid` (`comment_bbcode_uid`);

--
-- Indexes for table `phpbb_pa_custom`
--
ALTER TABLE `phpbb_pa_custom`
  ADD PRIMARY KEY (`custom_id`);

--
-- Indexes for table `phpbb_pa_files`
--
ALTER TABLE `phpbb_pa_files`
  ADD PRIMARY KEY (`file_id`),
  ADD KEY `file_catid` (`file_catid`);

--
-- Indexes for table `phpbb_pa_license`
--
ALTER TABLE `phpbb_pa_license`
  ADD PRIMARY KEY (`license_id`);

--
-- Indexes for table `phpbb_portal_config`
--
ALTER TABLE `phpbb_portal_config`
  ADD PRIMARY KEY (`config_name`);

--
-- Indexes for table `phpbb_posts`
--
ALTER TABLE `phpbb_posts`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `forum_id` (`forum_id`),
  ADD KEY `topic_id` (`topic_id`),
  ADD KEY `poster_id` (`poster_id`),
  ADD KEY `post_time` (`post_time`),
  ADD KEY `reporter_id` (`reporter_id`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_approve` (`post_approve`);

--
-- Indexes for table `phpbb_posts_text`
--
ALTER TABLE `phpbb_posts_text`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `phpbb_posts_text_history`
--
ALTER TABLE `phpbb_posts_text_history`
  ADD PRIMARY KEY (`th_id`),
  ADD KEY `th_post_id` (`th_post_id`);

--
-- Indexes for table `phpbb_privmsgs`
--
ALTER TABLE `phpbb_privmsgs`
  ADD PRIMARY KEY (`privmsgs_id`),
  ADD KEY `privmsgs_from_userid` (`privmsgs_from_userid`),
  ADD KEY `privmsgs_to_userid` (`privmsgs_to_userid`),
  ADD KEY `privmsgs_type` (`privmsgs_type`);

--
-- Indexes for table `phpbb_privmsgs_text`
--
ALTER TABLE `phpbb_privmsgs_text`
  ADD PRIMARY KEY (`privmsgs_text_id`);

--
-- Indexes for table `phpbb_profile_fields`
--
ALTER TABLE `phpbb_profile_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phpbb_quota_limits`
--
ALTER TABLE `phpbb_quota_limits`
  ADD PRIMARY KEY (`quota_limit_id`);

--
-- Indexes for table `phpbb_ranks`
--
ALTER TABLE `phpbb_ranks`
  ADD PRIMARY KEY (`rank_id`);

--
-- Indexes for table `phpbb_read_history`
--
ALTER TABLE `phpbb_read_history`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `topic_id` (`topic_id`),
  ADD KEY `forum_id` (`forum_id`);

--
-- Indexes for table `phpbb_search_results`
--
ALTER TABLE `phpbb_search_results`
  ADD PRIMARY KEY (`search_id`),
  ADD KEY `session_id` (`session_id`);

--
-- Indexes for table `phpbb_search_wordlist`
--
ALTER TABLE `phpbb_search_wordlist`
  ADD PRIMARY KEY (`word_text`),
  ADD KEY `word_id` (`word_id`);

--
-- Indexes for table `phpbb_search_wordmatch`
--
ALTER TABLE `phpbb_search_wordmatch`
  ADD KEY `post_id` (`post_id`),
  ADD KEY `word_id` (`word_id`);

--
-- Indexes for table `phpbb_sessions`
--
ALTER TABLE `phpbb_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `session_user_id` (`session_user_id`),
  ADD KEY `session_id_ip_user_id` (`session_id`,`session_ip`,`session_user_id`),
  ADD KEY `session_time` (`session_time`);

--
-- Indexes for table `phpbb_sessions_keys`
--
ALTER TABLE `phpbb_sessions_keys`
  ADD PRIMARY KEY (`key_id`,`user_id`),
  ADD KEY `last_login` (`last_login`);

--
-- Indexes for table `phpbb_shoutbox`
--
ALTER TABLE `phpbb_shoutbox`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sb_user_id` (`sb_user_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `phpbb_shoutbox_config`
--
ALTER TABLE `phpbb_shoutbox_config`
  ADD PRIMARY KEY (`config_name`);

--
-- Indexes for table `phpbb_smilies`
--
ALTER TABLE `phpbb_smilies`
  ADD PRIMARY KEY (`smilies_id`);

--
-- Indexes for table `phpbb_stats_config`
--
ALTER TABLE `phpbb_stats_config`
  ADD PRIMARY KEY (`config_name`);

--
-- Indexes for table `phpbb_stats_modules`
--
ALTER TABLE `phpbb_stats_modules`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `phpbb_themes`
--
ALTER TABLE `phpbb_themes`
  ADD PRIMARY KEY (`themes_id`);

--
-- Indexes for table `phpbb_themes_name`
--
ALTER TABLE `phpbb_themes_name`
  ADD PRIMARY KEY (`themes_id`);

--
-- Indexes for table `phpbb_topics`
--
ALTER TABLE `phpbb_topics`
  ADD PRIMARY KEY (`topic_id`),
  ADD KEY `forum_id` (`forum_id`),
  ADD KEY `topic_moved_id` (`topic_moved_id`),
  ADD KEY `topic_status` (`topic_status`),
  ADD KEY `topic_type` (`topic_type`),
  ADD KEY `topic_poster` (`topic_poster`),
  ADD KEY `topic_last_post_id` (`topic_last_post_id`),
  ADD KEY `topic_first_post_id` (`topic_first_post_id`),
  ADD KEY `topic_vote` (`topic_vote`);

--
-- Indexes for table `phpbb_topics_ignore`
--
ALTER TABLE `phpbb_topics_ignore`
  ADD PRIMARY KEY (`topic_id`,`user_id`);

--
-- Indexes for table `phpbb_topics_watch`
--
ALTER TABLE `phpbb_topics_watch`
  ADD KEY `topic_id` (`topic_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `notify_status` (`notify_status`);

--
-- Indexes for table `phpbb_topic_view`
--
ALTER TABLE `phpbb_topic_view`
  ADD PRIMARY KEY (`topic_id`,`user_id`);

--
-- Indexes for table `phpbb_users`
--
ALTER TABLE `phpbb_users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_session_time` (`user_session_time`),
  ADD KEY `user_level` (`user_level`),
  ADD KEY `user_lastvisit` (`user_lastvisit`),
  ADD KEY `user_active` (`user_active`);

--
-- Indexes for table `phpbb_users_warnings`
--
ALTER TABLE `phpbb_users_warnings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archive` (`archive`),
  ADD KEY `warning_viewed` (`warning_viewed`),
  ADD KEY `date` (`date`),
  ADD KEY `userid` (`userid`),
  ADD KEY `modid` (`modid`);

--
-- Indexes for table `phpbb_user_group`
--
ALTER TABLE `phpbb_user_group`
  ADD KEY `group_id` (`group_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_pending` (`user_pending`);

--
-- Indexes for table `phpbb_vote_desc`
--
ALTER TABLE `phpbb_vote_desc`
  ADD PRIMARY KEY (`vote_id`),
  ADD KEY `topic_id` (`topic_id`);

--
-- Indexes for table `phpbb_vote_results`
--
ALTER TABLE `phpbb_vote_results`
  ADD KEY `vote_option_id` (`vote_option_id`),
  ADD KEY `vote_id` (`vote_id`);

--
-- Indexes for table `phpbb_vote_voters`
--
ALTER TABLE `phpbb_vote_voters`
  ADD KEY `vote_id` (`vote_id`),
  ADD KEY `vote_user_id` (`vote_user_id`),
  ADD KEY `vote_user_ip` (`vote_user_ip`);

--
-- Indexes for table `phpbb_words`
--
ALTER TABLE `phpbb_words`
  ADD PRIMARY KEY (`word_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `phpbb_advertisement`
--
ALTER TABLE `phpbb_advertisement`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_attachments_desc`
--
ALTER TABLE `phpbb_attachments_desc`
  MODIFY `attach_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_banlist`
--
ALTER TABLE `phpbb_banlist`
  MODIFY `ban_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_categories`
--
ALTER TABLE `phpbb_categories`
  MODIFY `cat_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_disallow`
--
ALTER TABLE `phpbb_disallow`
  MODIFY `disallow_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_extensions`
--
ALTER TABLE `phpbb_extensions`
  MODIFY `ext_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_extension_groups`
--
ALTER TABLE `phpbb_extension_groups`
  MODIFY `group_id` mediumint(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_forbidden_extensions`
--
ALTER TABLE `phpbb_forbidden_extensions`
  MODIFY `ext_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_forum_prune`
--
ALTER TABLE `phpbb_forum_prune`
  MODIFY `prune_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_groups`
--
ALTER TABLE `phpbb_groups`
  MODIFY `group_id` mediumint(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_logs`
--
ALTER TABLE `phpbb_logs`
  MODIFY `id_log` mediumint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_pa_cat`
--
ALTER TABLE `phpbb_pa_cat`
  MODIFY `cat_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_pa_comments`
--
ALTER TABLE `phpbb_pa_comments`
  MODIFY `comments_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_pa_custom`
--
ALTER TABLE `phpbb_pa_custom`
  MODIFY `custom_id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_pa_files`
--
ALTER TABLE `phpbb_pa_files`
  MODIFY `file_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_pa_license`
--
ALTER TABLE `phpbb_pa_license`
  MODIFY `license_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_posts`
--
ALTER TABLE `phpbb_posts`
  MODIFY `post_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_posts_text_history`
--
ALTER TABLE `phpbb_posts_text_history`
  MODIFY `th_id` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_privmsgs`
--
ALTER TABLE `phpbb_privmsgs`
  MODIFY `privmsgs_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_profile_fields`
--
ALTER TABLE `phpbb_profile_fields`
  MODIFY `id` tinyint(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_quota_limits`
--
ALTER TABLE `phpbb_quota_limits`
  MODIFY `quota_limit_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_ranks`
--
ALTER TABLE `phpbb_ranks`
  MODIFY `rank_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_search_wordlist`
--
ALTER TABLE `phpbb_search_wordlist`
  MODIFY `word_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_shoutbox`
--
ALTER TABLE `phpbb_shoutbox`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_smilies`
--
ALTER TABLE `phpbb_smilies`
  MODIFY `smilies_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_themes`
--
ALTER TABLE `phpbb_themes`
  MODIFY `themes_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_topics`
--
ALTER TABLE `phpbb_topics`
  MODIFY `topic_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_topics_ignore`
--
ALTER TABLE `phpbb_topics_ignore`
  MODIFY `topic_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_users`
--
ALTER TABLE `phpbb_users`
  MODIFY `user_id` mediumint(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_users_warnings`
--
ALTER TABLE `phpbb_users_warnings`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_vote_desc`
--
ALTER TABLE `phpbb_vote_desc`
  MODIFY `vote_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `phpbb_words`
--
ALTER TABLE `phpbb_words`
  MODIFY `word_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;


-- Basic data

INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('upload_dir', 'files');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('upload_img', 'images/icon_clip.gif');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('topic_icon', 'images/icon_clip.gif');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('display_order', '0');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('max_filesize', '262144');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('attachment_quota', '52428800');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('max_filesize_pm', '262144');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('max_attachments', '10');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('max_attachments_pm', '1');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('disable_mod', '0');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('allow_pm_attach', '1');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('attachment_topic_review', '1');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('allow_ftp_upload', '0');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('show_apcp', '0');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('ftp_server', '');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('ftp_path', '');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('download_path', '');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('ftp_user', '');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('ftp_pass', '');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('img_display_inlined', '1');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('img_max_width', '0');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('img_max_height', '0');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('img_link_width', '0');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('img_link_height', '0');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('img_create_thumbnail', '1');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('img_min_thumb_filesize', '12000');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('img_imagick', '');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('default_upload_quota', '0');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('default_pm_quota', '0');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('ftp_pasv_mode', '1');
INSERT INTO phpbb_attachments_config (config_name, config_value) VALUES ('use_gd2', '0');

# -- categories
INSERT INTO phpbb_categories (cat_id, cat_title, cat_order, cat_main_type, cat_main, cat_desc) VALUES (1, 'Główna kategoria forum', 10, 'c', 0, 'Główna kategoria forum');
INSERT INTO phpbb_categories (cat_id, cat_title, cat_order, cat_main_type, cat_main, cat_desc) VALUES (3, 'Kategoria SubForów', 40, 'f', 2, '');

# -- config

INSERT INTO `phpbb_config` (`config_name`, `config_value`) VALUES
('server_name', 'nazwadomeny.pl'),
('server_port', '80'),
('script_path', '/'),
('cookie_domain', 'nazwadomeny.pl'),
('cookie_name', 'phpbb2mysql'),
('cookie_path', '/'),
('cookie_secure', '0'),
('board_disable', ''),
('session_length', '1200'),
('check_address', '0'),
('sitename', 'phpBB2 Turbo_UTF-8'),
('site_desc', 'phpBB2 Turbo_UTF-8'),
('allow_html', '0'),
('allow_html_tags', 'b,i,u,pre'),
('allow_bbcode', '1'),
('allow_smilies', '1'),
('allow_sig', '1'),
('allow_namechange', '0'),
('allow_avatar_local', '0'),
('allow_avatar_remote', '0'),
('allow_avatar_upload', '1'),
('override_user_style', '1'),
('posts_per_page', '15'),
('topics_per_page', '50'),
('hot_threshold', '20'),
('max_poll_options', '30'),
('max_sig_chars', '512'),
('max_inbox_privmsgs', '600'),
('max_sentbox_privmsgs', '600'),
('max_savebox_privmsgs', '600'),
('board_email_sig', 'phpBB2 Turbo_UTF-8'),
('board_email', 'admin@mojadomena.pl'),
('smtp_delivery', '0'),
('smtp_host', 'twoj.adresSMTP.pl'),
('smtp_username', 'admin@mojadomena.pl'),
('smtp_password', 'haslo'),
('require_activation', '1'),
('flood_interval', '5'),
('board_email_form', '0'),
('avatar_filesize', '81920'),
('avatar_max_width', '125'),
('avatar_max_height', '150'),
('avatar_path', 'images/avatars'),
('avatar_gallery_path', 'images/avatars/gallery'),
('smilies_path', 'images/smiles'),
('default_style', '4'),
('default_dateformat', 'Y-m-d, H:i'),
('board_timezone', '1.00'),
('prune_enable', '0'),
('privmsg_disable', '0'),
('gzip_compress', '1'),
('coppa_fax', ''),
('coppa_mail', ''),
('record_online_users', '119'),
('record_online_date', '1460958188'),
('version', '1.0'),
('allow_autologin', '1'),
('allow_photo_remote', '0'),
('allow_photo_upload', '1'),
('photo_filesize', '81920'),
('photo_max_height', '200'),
('photo_max_width', '200'),
('photo_path', 'images/photos'),
('allow_custom_rank', '0'),
('birthday_greeting', '1'),
('max_user_age', '112'),
('min_user_age', '1'),
('birthday_check_day', '7'),
('cload', '0'),
('cchat', '0'),
('cstat', '0'),
('cregist', '0'),
('cstyles', '0'),
('ccount', '0'),
('cchat2', '0'),
('cbirth', '1'),
('cpost', '1'),
('ctop', '1'),
('cfriend', '1'),
('cage', '1'),
('cjoin', '1'),
('cfrom', '1'),
('cposts', '1'),
('clevell', '0'),
('cleveld', '0'),
('cignore', '1'),
('cquick', '1'),
('csearch', '0'),
('cicq', '0'),
('cllogin', '1'),
('clevelp', '1'),
('cyahoo', '0'),
('cmsn', '0'),
('cjob', '0'),
('cinter', '1'),
('cemail', '0'),
('cbbcode', '0'),
('chtml', '0'),
('csmiles', '0'),
('clang', '0'),
('ctimezone', '1'),
('cbstyle', '0'),
('refresh', '0'),
('meta_keywords', 'phpBB2 Turbo_UTF-8'),
('meta_description', 'phpBB2 Turbo_UTF-8'),
('cavatar', '0'),
('clog', '0'),
('cagent', '1'),
('login_require', '0'),
('crestrict', '0'),
('validate', '1'),
('button_b', '1'),
('button_i', '1'),
('button_u', '1'),
('button_q', '1'),
('button_c', '1'),
('button_l', '1'),
('button_im', '1'),
('button_ur', '1'),
('button_ce', '1'),
('button_f', '1'),
('button_s', '1'),
('button_hi', '1'),
('color_box', '1'),
('size_box', '1'),
('glow_box', '1'),
('freak', '0'),
('allow_bbcode_quest', '1'),
('sql', ''),
('cregist_b', '0'),
('allow_custom_color', '0'),
('custom_color_view', '1'),
('custom_color_use', '0'),
('post_icon', '1'),
('auto_date', '1'),
('newest', '1'),
('download', '0'),
('ipview', '0'),
('show_badwords', '1'),
('album_gallery', '0'),
('address_whois', 'http://whois.domaintools.com/'),
('u_o_t_d', '1'),
('expire', '1'),
('expire_value', '0'),
('numer_gg', ''),
('haslo_gg', ''),
('block_time', '5'),
('max_login_error', '5'),
('min_password_len', '4'),
('force_complex_password', '0'),
('password_not_login', '0'),
('del_user_notify', '1'),
('require_aim', '0'),
('require_website', '0'),
('require_location', '0'),
('post_footer', '1'),
('graphic', '1'),
('max_sig_custom_rank', '30'),
('max_sig_location', '30'),
('custom_color_mod', '0'),
('custom_rank_mod', '0'),
('allow_sig_image', '1'),
('sig_images_path', 'images/signatures'),
('sig_image_filesize', '25600'),
('sig_image_max_width', '400'),
('sig_image_max_height', '20'),
('hide_viewed_admin', '0'),
('hide_edited_admin', '0'),
('who_viewed', '0'),
('who_viewed_admin', '0'),
('edit_time', '31'),
('gender', '1'),
('require_gender', '0'),
('main_admin_id', '2'),
('day_to_prune', '60'),
('banner_top', ''),
('banner_top_enable', '0'),
('banner_bottom', ''),
('banner_bottom_enable', '0'),
('header_enable', '0'),
('not_edit_admin', '0'),
('staff_forums', '0'),
('staff_enable', '0'),
('smilies_columns', '5'),
('smilies_rows', '8'),
('smilies_w_columns', '8'),
('generate_time', '0'),
('name_color', ''),
('desc_color', ''),
('mod_nick_color', ''),
('warnings_enable', '1'),
('mod_warnings', '1'),
('mod_edit_warnings', '0'),
('mod_value_warning', '3'),
('write_warnings', '3'),
('ban_warnings', '3'),
('expire_warnings', '180'),
('warnings_mods_public', '1'),
('viewtopic_warnings', '1'),
('board_msg_enable', '0'),
('board_msg', '---'),
('width_forum', '0'),
('width_table', '800'),
('width_color1', ''),
('width_color2', ''),
('table_border', '6'),
('rebuild_search', ''),
('generate_time_admin', '0'),
('r_a_r_time', '0'),
('visitors', '4'),
('email_return_path', 'admin@nazwa_domeny.pl'),
('email_from', 'admin@nazwa_domeny.pl'),
('poster_posts', '1'),
('sub_forum', '1'),
('sub_forum_over', '0'),
('split_cat', '0'),
('split_cat_over', '1'),
('last_topic_title', '1'),
('last_topic_title_over', '0'),
('last_topic_title_length', '24'),
('sub_level_links', '2'),
('sub_level_links_over', '0'),
('display_viewonline', '1'),
('display_viewonline_over', '0'),
('ignore_topics', '1'),
('topic_color', '1'),
('topic_color_all', '0'),
('topic_color_mod', '1'),
('allow_sig_image_img', '1'),
('last_dtable_notify', ''),
('rand_seed_last_update', ''),
('rand_seed', ''),
('report_no_guestes', '1'),
('report_no_auth_users', '6'),
('report_no_auth_groups', ''),
('report_disabled_users', ''),
('report_disabled_groups', ''),
('report_only_admin', '0'),
('report_popup_height', '250'),
('report_popup_width', '700'),
('report_popup_links_target', '2'),
('report_disable', '0'),
('sendmail_fix', '0'),
('allow_avatar', '0'),
('last_visitors_time', '300'),
('max_sig_chars_admin', '6'),
('max_sig_chars_mod', '3'),
('viewonline', '0'),
('restrict_smilies', '1'),
('topic_preview', '1'),
('not_anonymous_posting', '1'),
('not_anonymous_quickreply', '1'),
('max_smilies', '20'),
('portal_link', '0'),
('search_enable', '1'),
('overlib', '1'),
('admin_notify_reply', '1'),
('admin_notify_message', '0'),
('topic_start_date', '1'),
('topic_start_dateformat', 'd-m-y'),
('autorepair_tables', '1'),
('echange_banner', '0'),
('banners_list', '---'),
('split_messages', '1'),
('split_messages_admin', '1'),
('split_messages_mod', '1'),
('admin_html', '1'),
('jr_admin_html', '0'),
('mod_html', '0'),
('helped', '1'),
('del_notify_method', '0'),
('del_notify_enable', '0'),
('del_notify_choice', '1'),
('open_in_windows', '0'),
('title_explain', '1'),
('show_action_unlocked', '0'),
('show_action_locked', '0'),
('show_action_moved', '0'),
('show_action_expired', '0'),
('show_action_edited_by_others', '0'),
('show_action_edited_self', '0'),
('show_action_edited_self_all', '0'),
('allow_mod_delete_actions', '0'),
('show_rules', '0'),
('mod_spy', '0'),
('mod_spy_admin', '0'),
('post_overlib', '1'),
('ph_days', '30'),
('ph_len', '3'),
('ph_mod', '1'),
('ph_mod_delete', '0'),
('newestuser', 'Admin'),
('topiccount', ''),
('postcount', ''),
('usercount', ''),
('lastpost', ''),
('anonymous_simple', '0'),
('onmouse', '1'),
('birthday_data', ''),
('data', ''),
('last_resync', '0'),
('advert', ''),
('advert_foot', ''),
('view_ad_by', '1'),
('advert_width', '150'),
('advert_separator', ' &bull; '),
('advert_separator_l', '<br /><hr />'),
('adv_person_time', '10'),
('group_rank_hack_version', '1'),
('disable_type', ''),
('rh_without_days', '5'),
('rh_max_posts', '1000'),
('public_category', ''),
('last_visitors_time_count', '0'),
('search_keywords_max', '10'),
('protection_get', '1'),
('last_prune', ''),
('board_startdate', NOW()),
('default_lang', 'polish'),
('xs_auto_compile', '1'),
('xs_auto_recompile', '1'),
('xs_use_cache', '1'),
('xs_php', 'php'),
('xs_def_template', 'Silueta'),
('xs_check_switches', '1'),
('xs_warn_includes', '1'),
('xs_ftp_host', ''),
('xs_ftp_login', ''),
('xs_ftp_path', ''),
('xs_downloads_count', '0'),
('xs_downloads_default', '0'),
('xs_shownav', '1'),
('xs_template_time', '0'),
('xs_version', '7'),
('db_backup_enable', '0'),
('db_backup_copies', ''),
('db_backup_search', '0'),
('db_backup_rh', '0'),
('db_backup_time', ''),
('avatar_default', '1'),
('avatar_default_url', 'images/czolem_nowy_uzytkowniku.png'),
('url_post_days', '7'),
('url_post_posts', '10');

# -- extension_groups
INSERT INTO phpbb_extension_groups (group_id, group_name, cat_id, allow_group, download_mode, upload_icon, max_filesize, forum_permissions) VALUES (1,'Images',1,1,1,'',0,'');
INSERT INTO phpbb_extension_groups (group_id, group_name, cat_id, allow_group, download_mode, upload_icon, max_filesize, forum_permissions) VALUES (2,'Archives',0,1,1,'',0,'');
INSERT INTO phpbb_extension_groups (group_id, group_name, cat_id, allow_group, download_mode, upload_icon, max_filesize, forum_permissions) VALUES (3,'Plain Text',0,0,1,'',0,'');
INSERT INTO phpbb_extension_groups (group_id, group_name, cat_id, allow_group, download_mode, upload_icon, max_filesize, forum_permissions) VALUES (4,'Documents',0,0,1,'',0,'');
INSERT INTO phpbb_extension_groups (group_id, group_name, cat_id, allow_group, download_mode, upload_icon, max_filesize, forum_permissions) VALUES (5,'Real Media',0,0,2,'',0,'');
INSERT INTO phpbb_extension_groups (group_id, group_name, cat_id, allow_group, download_mode, upload_icon, max_filesize, forum_permissions) VALUES (6,'Streams',2,0,1,'',0,'');
INSERT INTO phpbb_extension_groups (group_id, group_name, cat_id, allow_group, download_mode, upload_icon, max_filesize, forum_permissions) VALUES (7,'Flash Files',3,0,1,'',0,'');

# -- extensions
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (1, 1, 'gif', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (2, 1, 'png', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (3, 1, 'jpeg', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (4, 1, 'jpg', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (5, 1, 'tif', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (6, 1, 'tga', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (7, 2, 'gtar', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (8, 2, 'gz', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (9, 2, 'tar', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (10, 2, 'zip', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (11, 2, 'rar', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (12, 2, 'ace', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (13, 3, 'txt', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (14, 3, 'c', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (15, 3, 'h', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (16, 3, 'cpp', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (17, 3, 'hpp', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (18, 3, 'diz', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (19, 4, 'xls', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (20, 4, 'doc', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (21, 4, 'dot', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (22, 4, 'pdf', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (23, 4, 'ai', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (24, 4, 'ps', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (25, 4, 'ppt', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (26, 5, 'rm', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (27, 6, 'wma', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (31, 6, 'avi', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (32, 6, 'mpg', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (33, 6, 'mpeg', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (34, 6, 'mp3', '');
INSERT INTO phpbb_extensions (ext_id, group_id, extension, comment) VALUES (35, 6, 'wav', '');

# -- forbidden_extensions
INSERT INTO phpbb_forbidden_extensions (ext_id, extension) VALUES (1,'php');
INSERT INTO phpbb_forbidden_extensions (ext_id, extension) VALUES (2,'php3');
INSERT INTO phpbb_forbidden_extensions (ext_id, extension) VALUES (3,'php4');
INSERT INTO phpbb_forbidden_extensions (ext_id, extension) VALUES (4,'phtml');
INSERT INTO phpbb_forbidden_extensions (ext_id, extension) VALUES (5,'pl');
INSERT INTO phpbb_forbidden_extensions (ext_id, extension) VALUES (6,'asp');
INSERT INTO phpbb_forbidden_extensions (ext_id, extension) VALUES (7,'cgi');

# -- forums
INSERT INTO phpbb_forums (forum_id, cat_id, forum_name, forum_desc, forum_status, forum_order, forum_posts, forum_topics, forum_last_post_id, prune_next, prune_enable, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_globalannounce, auth_vote, auth_pollcreate, auth_attachments, auth_download, password, forum_sort, forum_color, forum_link, forum_link_internal, forum_link_hit_count, forum_link_hit, main_type, forum_moderate, no_count) VALUES (1, 1, 'Testowe forum', 'Testowe forum.', 0, 20, 1, 1, 5, NULL, 0, 0, 0, 1, 0, 1, 1, 3, 3, 3, 1, 1, 1, 1, '', 'SORT_FPDATE', '', '', 0, 0, 0, 'c', 0, 0);
INSERT INTO phpbb_forums (forum_id, cat_id, forum_name, forum_desc, forum_status, forum_order, forum_posts, forum_topics, forum_last_post_id, prune_next, prune_enable, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_globalannounce, auth_vote, auth_pollcreate, auth_attachments, auth_download, password, forum_sort, forum_color, forum_link, forum_link_internal, forum_link_hit_count, forum_link_hit, main_type, forum_moderate, no_count) VALUES (2, 1, 'Test SubForów', 'W tym forum, znajdują się dwa testowe fora, można w nim również pisać tematy', 0, 30, 1, 1, 3, NULL, 0, 0, 0, 1, 0, 1, 1, 3, 3, 3, 1, 1, 1, 1, '', 'SORT_FPDATE', '', '', 0, 0, 0, 'c', 0, 0);
INSERT INTO phpbb_forums (forum_id, cat_id, forum_name, forum_desc, forum_status, forum_order, forum_posts, forum_topics, forum_last_post_id, prune_next, prune_enable, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_globalannounce, auth_vote, auth_pollcreate, auth_attachments, auth_download, password, forum_sort, forum_color, forum_link, forum_link_internal, forum_link_hit_count, forum_link_hit, main_type, forum_moderate, no_count) VALUES (3, 3, 'Testowe podforum 1', 'Testowe podforum 1', 0, 50, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 1, 1, 3, 3, 3, 1, 1, 1, 1, '', 'SORT_FPDATE', '', '', 0, 0, 0, 'c', 0, 0);
INSERT INTO phpbb_forums (forum_id, cat_id, forum_name, forum_desc, forum_status, forum_order, forum_posts, forum_topics, forum_last_post_id, prune_next, prune_enable, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_globalannounce, auth_vote, auth_pollcreate, auth_attachments, auth_download, password, forum_sort, forum_color, forum_link, forum_link_internal, forum_link_hit_count, forum_link_hit, main_type, forum_moderate, no_count) VALUES (4, 3, 'Testowe podforum 2', 'Testowe podforum 2', 0, 60, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 1, 1, 3, 3, 3, 1, 1, 1, 1, '', 'SORT_FPDATE', '', '', 0, 0, 0, 'c', 0, 0);

# -- groups
INSERT INTO phpbb_groups (group_id, group_name, group_description, group_single_user) VALUES (1, 'Anonymous', 'Personal User', 1);
INSERT INTO phpbb_groups (group_id, group_name, group_description, group_single_user) VALUES (2, 'Admin', 'Personal User', 1);

# -- pa_*
INSERT INTO phpbb_pa_cat VALUES (1, 'Test Category', 'Test category', 0, 0, 0, 1);
INSERT INTO phpbb_pa_settings (settings_id, settings_dbname, settings_sitename, settings_dburl, settings_topnumber, settings_homeurl, settings_newdays, settings_stats, settings_viewall, settings_showss, settings_disable, allow_html, allow_bbcode, allow_smilies, allow_comment_links, no_comment_link_message, allow_comment_images, no_comment_image_message, max_comment_chars, directly_linked) VALUES ('1', 'Download Index', 'My Site', 'http://yoursite_URL/phpBB2', '10', 'http://yoursite_URL', '5', '0', '1', '1', '0', '0', '1', '1', '0', 'No links please', '0', 'No images please', '1000', '0');

# -- posts
INSERT INTO phpbb_posts (post_id, topic_id, forum_id, poster_id, post_time, poster_ip, post_username, enable_bbcode, enable_html, enable_smilies, enable_sig, post_edit_time, post_edit_count, post_attachment, user_agent, post_icon, post_expire, post_marked, post_approve) VALUES (3, 2, 2, 2, 1562453193, '7f000001', '', 1, 0, 1, 0, NULL, 0, 0, '', 0, 0, 'n', 1);
INSERT INTO phpbb_posts (post_id, topic_id, forum_id, poster_id, post_time, poster_ip, post_username, enable_bbcode, enable_html, enable_smilies, enable_sig, post_edit_time, post_edit_count, post_attachment, user_agent, post_icon, post_expire, post_marked, post_approve) VALUES (5, 4, 1, 2, 1562453193, '7f000001', '', 1, 0, 0, 0, NULL, 0, 0, '', 2, 0, 'n', 1);

# -- posts_text
INSERT INTO phpbb_posts_text (post_id, bbcode_uid, post_subject, post_text) VALUES (3, 'b63934e592', 'Testowy temat w SubForum', 'Testowy temat w SubForum');
INSERT INTO phpbb_posts_text (post_id, bbcode_uid, post_subject, post_text) VALUES (5, 'bcf3b5262c', 'Witaj na forum bb2 Turbo', 'Prawidłowo zainstalowałeś [URL=http://bb2.cout.pl]bb2 Turbo[/URL]');

# -- quota_limits
INSERT INTO phpbb_quota_limits (quota_limit_id, quota_desc, quota_limit) VALUES (1, 'Low', 262144);
INSERT INTO phpbb_quota_limits (quota_limit_id, quota_desc, quota_limit) VALUES (2, 'Medium', 2097152);
INSERT INTO phpbb_quota_limits (quota_limit_id, quota_desc, quota_limit) VALUES (3, 'High', 5242880);

# -- topics
INSERT INTO phpbb_topics (topic_id, forum_id, topic_title, topic_poster, topic_time, topic_views, topic_replies, topic_status, topic_vote, topic_type, topic_first_post_id, topic_last_post_id, topic_moved_id, topic_attachment, topic_icon, topic_expire, topic_color, topic_title_e, topic_action, topic_action_user, topic_action_date, topic_tree_width) VALUES (2, 2, 'Testowy temat w SubForum', 2, 1064458873, 0, 0, 0, 0, 0, 3, 3, 0, 0, 0, 0, '', '', 0, 0, 0, 0);
INSERT INTO phpbb_topics (topic_id, forum_id, topic_title, topic_poster, topic_time, topic_views, topic_replies, topic_status, topic_vote, topic_type, topic_first_post_id, topic_last_post_id, topic_moved_id, topic_attachment, topic_icon, topic_expire, topic_color, topic_title_e, topic_action, topic_action_user, topic_action_date, topic_tree_width) VALUES (4, 1, 'Witaj na forum bb2 Turbo', 2, 1065136668, 0, 0, 0, 0, 0, 5, 5, 0, 0, 2, 0, 'green', '', 0, 0, 0, 0);

# -- users
INSERT INTO phpbb_users (user_id, username, user_level, user_regdate, user_password, user_email, user_icq, user_website, user_occ, user_from, user_interests, user_sig, user_viewemail, user_style, user_aim, user_yim, user_msnm, user_posts, user_attachsig, user_allowsmile, user_allowhtml, user_allowbbcode, user_allow_pm, user_notify_pm, user_allow_viewonline, user_rank, user_avatar, user_lang, user_timezone, user_actkey, user_newpasswd, user_notify, user_active) VALUES ( -1, 'Anonymous', 0, 0, '', '', '', '', '', '', '', '', 0, NULL, '', '', '', 0, 1, 1, 1, 1, 0, 1, 1, 0, '', 'polish', 1.00, '', '', 0, 0);
INSERT INTO phpbb_users (user_id, username, user_level, user_regdate, user_password, user_email, user_icq, user_website, user_occ, user_from, user_interests, user_sig, user_viewemail, user_style, user_aim, user_yim, user_msnm, user_posts, user_attachsig, user_allowsmile, user_allowhtml, user_allowbbcode, user_allow_pm, user_notify_pm, user_popup_pm, user_allow_viewonline, user_rank, user_avatar, user_lang, user_timezone, user_actkey, user_newpasswd, user_notify, user_active) VALUES ( 2, 'Admin', 1, 0, '21232f297a57a5a743894a0e4a801fc3', 'admin@yourdomain.com', '', '', '', '', '', '', 1, 1, '', '', '', 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, '', '', 1.00, '', '', 0, 1);

# -- user_group
INSERT INTO phpbb_user_group (group_id, user_id, user_pending) VALUES (1, -1, 0);
INSERT INTO phpbb_user_group (group_id, user_id, user_pending) VALUES (2, 2, 0);

# -- vote

# -- ranks
INSERT INTO phpbb_ranks (rank_id, rank_title, rank_min, rank_special, rank_image) VALUES ( 1, 'Administrator', -1, 1, NULL);

# -- stats_config
INSERT INTO phpbb_stats_config (config_name, config_value) VALUES ('install_date', 'time()');
INSERT INTO phpbb_stats_config (config_name, config_value) VALUES ('return_limit', '10');
INSERT INTO phpbb_stats_config (config_name, config_value) VALUES ('version', '2.1.3');
INSERT INTO phpbb_stats_config (config_name, config_value) VALUES ('modules_dir', 'stat_modules');
INSERT INTO phpbb_stats_config (config_name, config_value) VALUES ('page_views', '0');

# -- stats_modules
-- Brak w tej wersji

# -- shoutbox_config
INSERT INTO `phpbb_shoutbox_config` (`config_name`, `config_value`) VALUES
('allow_guest_view', '1'),
('allow_guest', '0'),
('allow_users_view', '1'),
('allow_users', '1'),
('allow_delete_all', '0'),
('allow_delete', '1'),
('allow_delete_m', '1'),
('allow_edit_all', '1'),
('allow_edit', '1'),
('allow_edit_m', '0'),
('allow_bbcode', '1'),
('allow_smilies', '1'),
('links_names', '0'),
('make_links', '1'),
('count_msg', '40'),
('delete_days', '30'),
('text_lenght', '600'),
('word_lenght', '80'),
('date_format', 'D H:i'),
('date_on', '1'),
('shoutbox_on', '1'),
('shout_width', '750'),
('shout_height', '240'),
('banned_user_id', ''),
('banned_user_id_view', '6'),
('shout_refresh', '7'),
('sb_group_sel', 'all'),
('usercall', '1'),
('shoutbox_smilies', '1');

# -- smilies
INSERT INTO `phpbb_smilies` (`smilies_id`, `code`, `smile_url`, `emoticon`, `smile_order`) VALUES
(42, ';-)', 'icon_wink.gif', '', 1),
(43, ':-)', 'icon_smile.gif', '', 2),
(44, ':-D', 'icon_biggrin.gif', '', 3),
(45, ':-DD', 'icon_mrgreen.gif', '', 4),
(46, ':-P', 'icon_razz.gif', '', 5),
(47, ':-&gt;', 'icon_smile2.gif', '', 6),
(48, ':-o', 'icon_surprised.gif', '', 7),
(49, ':lol:', 'icon_lol.gif', '', 8),
(50, ':-(', 'icon_sad.gif', '', 9),
(51, ':-|', 'icon_neutral.gif', '', 10),
(52, ':-/', 'icon_curve.gif', '', 11),
(53, ':-?', 'icon_confused.gif', '', 12),
(54, ':cry:', 'icon_cry.gif', '', 13),
(55, ':oops:', 'icon_redface.gif', '', 14),
(56, '8-)', 'icon_cool.gif', '', 15),
(57, ':evil:', 'icon_evil.gif', '', 16),
(58, ':roll:', 'icon_rolleyes.gif', '', 17),
(59, ':!:', 'icon_exclaim.gif', '', 18),
(60, ':?:', 'icon_question.gif', '', 19),
(61, ':idea:', 'icon_idea.gif', '', 20),
(62, ':arrow:', 'icon_arrow.gif', '', 21),
(63, 'xDD', 'mam_tylko_dwa_zeby_ale_za_to_rowne.gif', '', 22),
(64, ':mialem_dynamit_w_paszczy:', 'wybuch_dynamitu_spowodowal_nieodwracalne_ubytki_w_moim_uzebieniu.GIF', '', 23),
(65, ':crazy:', 'crazy.gif', '', 24);

# -- themes

INSERT INTO `phpbb_themes` (`themes_id`, `template_name`, `style_name`, `head_stylesheet`, `body_background`, `body_bgcolor`, `body_text`, `body_link`, `body_vlink`, `body_alink`, `body_hlink`, `tr_color1`, `tr_color2`, `tr_color3`, `tr_color_helped`, `tr_class1`, `tr_class2`, `tr_class3`, `th_color1`, `th_color2`, `th_color3`, `th_class1`, `th_class2`, `th_class3`, `td_color1`, `td_color2`, `td_color3`, `td_class1`, `td_class2`, `td_class3`, `fontface1`, `fontface2`, `fontface3`, `fontsize1`, `fontsize2`, `fontsize3`, `fontcolor1`, `fontcolor2`, `fontcolor3`, `fontcolor_admin`, `fontcolor_jradmin`, `fontcolor_mod`, `factive_color`, `faonmouse_color`, `faonmouse2_color`, `span_class1`, `span_class2`, `span_class3`, `img_size_poll`, `img_size_privmsg`) VALUES
(1, 'Silueta', 'Silueta', 'Silueta.css', '', 'E5E5E5', '000000', '006699', '5493B4', '', 'DD6900', 'EFEFEF', 'DEE3E7', 'D1D7DC', 'F0EDDE', '', '', '', '98AAB1', '006699', 'FFFFFF', 'cellpic1.gif', 'cellpic3.gif', 'cellpic2.jpg', 'FAFAFA', 'FFFFFF', '', 'row1', 'row2', '', 'Verdana, Arial, Helvetica, sans-serif', 'Trebuchet MS', 'Courier, \'Courier New\', sans-serif', 10, 11, 12, '444444', '006600', 'ffa34f', '', '', '', 'F9F9F0', 'EFEAE7', 'F5F2F0', '', '', '', 0, 0),
(2, 'SiluetaMobile', 'SiluetaMobile', 'SiluetaMobile.css', '', 'E5E5E5', '000000', '006699', '5493B4', '', 'DD6900', 'EFEFEF', 'DEE3E7', 'D1D7DC', 'F0EDDE', '', '', '', '98AAB1', '006699', 'FFFFFF', 'cellpic1.gif', 'cellpic3.gif', 'cellpic2.jpg', 'FAFAFA', 'FFFFFF', '', 'row1', 'row2', '', 'Verdana, Arial, Helvetica, sans-serif', 'Trebuchet MS', 'Courier, \'Courier New\', sans-serif', 10, 11, 12, '444444', '006600', 'ffa34f', '', '', '', 'F9F9F0', 'EFEAE7', 'F5F2F0', '', '', '', 0, 0);

# -- themes_name

INSERT INTO `phpbb_themes_name` (`themes_id`, `tr_color1_name`, `tr_color2_name`, `tr_color3_name`, `tr_class1_name`, `tr_class2_name`, `tr_class3_name`, `th_color1_name`, `th_color2_name`, `th_color3_name`, `th_class1_name`, `th_class2_name`, `th_class3_name`, `td_color1_name`, `td_color2_name`, `td_color3_name`, `td_class1_name`, `td_class2_name`, `td_class3_name`, `fontface1_name`, `fontface2_name`, `fontface3_name`, `fontsize1_name`, `fontsize2_name`, `fontsize3_name`, `fontcolor1_name`, `fontcolor2_name`, `fontcolor3_name`, `span_class1_name`, `span_class2_name`, `span_class3_name`) VALUES
(1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
