<?php

$lang['Private_Messages'] = 'Prywatne wiadomo¶ci';
$lang['To'] = 'Do';
$lang['Subject'] = 'Temat';
$lang['Sent_Date'] = 'Data wysłania';
$lang['Explain_Modes'] = 'There are six modes to choose from.  You may choose which type you wish to view and display.  Everything will be sorted by date and your choice of Ascending or Descending.';
$lang['Mode_1'] = 'Odczytane wiadomo¶ci';
$lang['Mode_2'] = 'Nowe wiadomo¶ci';
$lang['Mode_3'] = 'Wysłane wiadomo¶ci';
$lang['Mode_4'] = 'Wiadomo¶ci odczytane i zapisane w skrzynkach';
$lang['Mode_5'] = 'Wiadomo¶ci zapisane';
$lang['Mode_6'] = 'Nieprzeczytane wiadomo¶ci';
$lang['Deleted_Topic'] = 'Skasuj temat z ID %d'; //%d = topic id
$lang['Delete'] = 'Skasuj';

// Nie ma w czeskim i slowackim
$lang['cpriv'] = 'Ta opcja jest dostępna tylko dla głównego (ustawionego) admina';

?>