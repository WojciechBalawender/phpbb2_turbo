Subject: Nowe Konto
Charset: utf-8
Witamy,

Konto o nazwie "{USERNAME}" zostało deaktywowane lub nowo utworzone, sprawdź szczegóły tego użytkownika (jeśli to konieczne) i aktywować konto używając poniższego odnośnika:

{U_ACTIVATE}

{EMAIL_SIG}