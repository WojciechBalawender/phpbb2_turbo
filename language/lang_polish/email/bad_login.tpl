Subject: Konto na "{SITENAME}" zostało czasowo zablokowane.
Charset: utf-8
Witaj {USER},

wykryliśmy wielokrotne próby logowania ({BAD_LOGINS}) na twoje konto z użyciem błędnego hasła.
Zablokowaliśmy je w celach bezpieczeństwa. Konto będzie z powrotem aktywne {BLOCK_UNTIL}.

Jeśli to nie Ty próbowałeś/aś się logować, koniecznie skontaktuj się z Gazdą.
Jeśli zapomniałeś swojego hasła, użyj formularza odzyskiwania hasła.

{EMAIL_SIG}