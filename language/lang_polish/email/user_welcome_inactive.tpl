Subject: Witamy na {SITENAME}
Charset: utf-8
{WELCOME_MSG}

Twoje konto jest obecnie nieaktywne. Nie możesz go używać zanim nie odwiedzisz poniższej strony:

{U_ACTIVATE}

Nie zapomnij swojego hasła, Jeśli zapomnisz, możesz otrzymać nowe, które będzie trzeba aktywować w identyczny sposób jak to konto.

Dziękujemy za rejestrację.

{EMAIL_SIG}
