Subject: Nadeszła nowa Prywatna Wiadomość
Charset: utf-8
Witaj {USERNAME},

W twojej skrzynce pojawiła się nowa Prywatna Wiadomość od {POSTER_USERNAME} na "{SITENAME}". Możesz zobaczyć nową wiadomość klikając na poniższy odnośnik:
{U_INBOX}

Pamiętaj, że możesz zawsze wyłączyć powiadamianie o nowych wiadomościach zmieniając odpowiednie ustawienia w Twoim profilu.

{EMAIL_SIG}
