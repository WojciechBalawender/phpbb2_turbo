Subject: Usunięcie konta na {SITENAME} 
Charset: utf-8
Witaj {USERNAME}, 

rozpatrzyliśmy prośbę i usunęliśmy konto z {SITENAME}
Mamy nadzieję, że kiedyś jeszcze do Nas wrócisz. :)

{EMAIL_SIG}
