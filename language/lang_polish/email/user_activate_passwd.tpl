Subject: Aktywacja nowego hasła
Charset: utf-8
Witaj {USERNAME}

Otrzymujesz ten email ponieważ Ty (albo ktoś podający się za Ciebie) poprosił o nowe hasło dla Twojego konta na {SITENAME}. Jeżeli nie prosiłeś(aś) o ten email zignoruj go, a jeśli otrzymasz go ponownie skontaktuj się z administratorem.

Aby skorzystać z nowego hasła musisz je aktywować. Aby to zrobić kliknij odnośnik poniżej.

{U_ACTIVATE}

Gdy to zrobisz będziesz mógł/mogła się logować korzystając z nowego hasła

Hasło: {PASSWORD}
 

Możesz oczywiście zmienić to hasło w swoim profilu. Jeżeli będziesz miał/a problemy skontaktuj się z administratorem.

{EMAIL_SIG}
