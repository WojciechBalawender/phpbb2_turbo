Subject: Konto Aktywowane
Charset: utf-8
Witaj {USERNAME},

Twoje konto na "{SITENAME}" zostało właśnie aktywowane, możesz zalogować się korzystając z loginu i hasła, podanych przy rejestracji.

{EMAIL_SIG}
