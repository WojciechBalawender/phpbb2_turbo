Subject: Reaktywuj swoje konto!
Charset: utf-8
Witaj {USERNAME},

Twoje konto na "{SITENAME}" zostało deaktywowane, prawdopodobnie w związku ze zmianami jakie dokonałeś(aś) w swoim profilu. Aby ponownie aktywować Twoje konto musisz kliknąć poniższy odnośnik:

{U_ACTIVATE}

{EMAIL_SIG}
