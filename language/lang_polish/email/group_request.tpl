Subject: Prośba o przyłączenie do grupy "{GROUP_NAME}"
Charset: utf-8
Drogi {GROUP_MODERATOR},

Użytkownik {USERNAME} poprosił o członkostwo w Twojej grupie na {SITENAME}.
Aby przyjąć lub odrzucić tą prośbę odwiedź poniższą stronę:

{U_GROUPCP}

{EMAIL_SIG}
