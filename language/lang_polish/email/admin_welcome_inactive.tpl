Subject: Witamy na "{SITENAME}"
Charset: utf-8
{WELCOME_MSG}

Twoje konto jest obecnie nieaktywne, musi zostać najpierw aktywowane przez administratora, zanim będzie można się zalogować. Otrzymasz kolejny email gdy to nastąpi.

Nie zapomnij swojego hasła, Jeśli zapomnisz hasła możesz otrzymać nowe, które będzie można aktywować w identyczny sposób jak to konto.
Dziękujemy za rejestrację.

{EMAIL_SIG}
