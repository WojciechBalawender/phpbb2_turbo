Subject: Przyjęcie do Grupy
Charset: utf-8
Gratulacje,

Twoje zgłoszenie członkostwa w Grupie "{GROUP_NAME}" na {SITENAME} zostało zaakceptowane.
Zostało to przeprowadzone przez moderatora grupy lub administratora.

Możesz zobaczyć informację o Twojej grupie tutaj: {U_GROUPCP}

{EMAIL_SIG}
