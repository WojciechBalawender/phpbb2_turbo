Subject: Witamy na {SITENAME}
Charset: utf-8
{WELCOME_MSG}

Twoje konto zostało pomyślnie zarejestrowane. 
Możesz teraz wrzucać swoje posty do strumienia razem z Nami! 
Zapamiętaj swoje hasło (jeśli zapomnisz, skorzystaj z opcji przypomnienia hasła).

Dzięki!

{EMAIL_SIG}
