Subject: Zgłoszenie postu na "{SITENAME}".
Charset: utf-8

{USERNAME}!

{REPORTER} zglosił post użytkownika: {POSTER}.
Możesz zobaczyć ten post:
{U_POST}

Lub listę wszystkich zgłoszonych postów:
{U_REPORTS}

Powiadomienia możesz wyłączyć w swoim profilu:
{EMAIL_SIG}