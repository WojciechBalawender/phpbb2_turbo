Subject: Powiadomienie o Odpowiedzi - {TOPIC_TITLE}
Charset: utf-8
Witaj,

Otrzymujesz ten email ponieważ obserwujesz temat "{TOPIC_TITLE}" na {SITENAME}. W tym temacie użytkownik {NOTIFICATION_USERNAME} napisał odpowiedź. Możesz skorzystać z poniższego odnośnika aby zobaczyć odpowiedzi. Nie dostaniesz więcej powiadomień dopóki nie odwiedzisz tego tematu.
{U_TOPIC}

{EMAIL_SIG}
