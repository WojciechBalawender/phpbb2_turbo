Subject: Konto użytkownika na {SITENAME}
Charset: utf-8

Drogi {USERNAME},

Wiadomo¶ć wygenerowana automatycznie. Twoje konto na {SITENAME} zostało usunięte.

Żeby dowiedziec się więcej, skontaktuj się z Administratorem.

{EMAIL_SIG}