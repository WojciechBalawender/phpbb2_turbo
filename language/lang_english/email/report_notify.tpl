Subject: A user has reported a post
Charset: utf-8

Hello {USERNAME}!

At the moment {REPORTER} has reported a post from {POSTER} on {SITENAME}.

Click on follow link to come direct to the reported post.
{U_POST}

Or click on follow link to come to the report list.
{U_REPORTS}

Remember that you can always choose not to be notified of new reports by changing the appropriate setting in your profile.

{EMAIL_SIG}