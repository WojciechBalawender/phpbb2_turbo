<?php

/***************************************************************************
 *                                common.php
 *                            -------------------
 *   begin                : Saturday, Feb 23, 2001
 *   copyright            : (C) 2001 The phpBB Group
 *   email                : support@phpbb.com
 *   modification         : (C) 2005 Przemo www.przemo.org/phpBB2/
 *   date modification    : ver. 1.12.5 2005/09/20 12:34
 *
 *   $Id: common.php,v 1.74.2.21 2005/10/31 07:31:06 acydburn Exp $
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

$phpbb_root_dir = __DIR__ . '/';
$sql_cache_enable = 1;
$show_queries = 0;

if ( !defined('IN_PHPBB') )
{
	die('Hacking attempt');
}

$time_start = microtime(1);

//
//ini_set('display_errors', true);
//error_reporting  (E_ALL);
//error_reporting  (E_ERROR | E_WARNING | E_PARSE); // This will NOT report uninitialized variables

/**
 * Mobile detection
 * https://www.codespeedy.com/simple-php-code-to-detect-mobile-device/
 */
 
 
 /**
 * Obudowana funkcja count dla pozbycia sie warningow
 */

function php7_count($ob)
{
	return is_array($ob) ? count($ob) : 0;
}

function isMobileDevice()
{
    $re = "/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i";
    return preg_match($re, $_SERVER["HTTP_USER_AGENT"]);
}

/**
 * Bot detection
 * https://stackoverflow.com/questions/677419/how-to-detect-search-engine-bots-with-php
 */
function isBot()
{
    $re = '/BotLink|bingbot|AhrefsBot|ahoy|AlkalineBOT|anthill|appie|arale|araneo|AraybOt'.
        '|ariadne|arks|ATN_Worldwide|Atomz|bbot|Bjaaland|Ukonline|borg\-bot\/0\.9|boxseabot'.
        '|bspider|calif|christcrawler|CMC\/0\.01|combine|confuzzledbot|CoolBot|cosmos'.
        '|Internet Cruiser Robot|cusco|cyberspyder|cydralspider|desertrealm, desert realm'.
        '|digger|DIIbot|grabber|downloadexpress|DragonBot|dwcp|ecollector|ebiness|elfinbot'.
        '|esculapio|esther|fastcrawler|FDSE|FELIX IDE|ESI|fido|H�m�h�kki|KIT\-Fireball|fouineur'.
        '|Freecrawl|gammaSpider|gazz|gcreep|golem|googlebot|griffon|Gromit|gulliver|gulper'.
        '|hambot|havIndex|hotwired|htdig|iajabot|INGRID\/0\.1|Informant|InfoSpiders'.
        '|inspectorwww|irobot|Iron33|JBot|jcrawler|Teoma|Jeeves|jobo|image\.kapsi\.net'.
        '|KDD\-Explorer|ko_yappo_robot|label\-grabber|larbin|legs|Linkidator|linkwalker'.
        '|Lockon|logo_gif_crawler|marvin|mattie|mediafox|MerzScope|NEC\-MeshExplorer'.
        '|MindCrawler|udmsearch|moget|Motor|msnbot|muncher|muninn|MuscatFerret|MwdSearch'.
        '|sharp\-info\-agent|WebMechanic|NetScoop|newscan\-online|ObjectsSearch|Occam'.
        '|Orbsearch\/1\.0|packrat|pageboy|ParaSite|patric|pegasus|perlcrawler|phpdig'.
        '|piltdownman|Pimptrain|pjspider|PlumtreeWebAccessor|PortalBSpider|psbot'.
        '|Getterrobo\-Plus|Raven|RHCS|RixBot|roadrunner|Robbie|robi|RoboCrawl|robofox'.
        '|Scooter|Search\-AU|searchprocess|Senrigan|Shagseeker|sift|SimBot|Site Valet'.
        '|skymob|SLCrawler\/2\.0|slurp|ESI|snooper|solbot|speedy|spider_monkey'.
        '|SpiderBot\/1\.0|spiderline|nil|suke|http:\/\/www\.sygol\.com|tach_bw|TechBOT'.
        '|templeton|titin|topiclink|UdmSearch|urlck|Valkyrie libwww\-perl|verticrawl'.
        '|Victoria|void\-bot|Voyager|VWbot_K|crawlpaper|wapspider|WebBandit\/1\.0'.
        '|webcatcher|T\-H\-U\-N\-D\-E\-R\-S\-T\-O\-N\-E|WebMoose|webquest|webreaper'.
        '|webs|webspider|WebWalker|wget|winona|whowhere|wlm|WOLP|WWWC|none|XGET'.
        '|Nederland\.zoek|AISearchBot|woriobot|NetSeer|Nutch|YandexBot|YandexMobileBot'.
        '|SemrushBot|FatBot|MJ12bot|DotBot|AddThis|baiduspider|SeznamBot|mod_pagespeed'.
        '|CCBot|openstat.ru\/Bot|m2e/i';
    return preg_match($re, $_SERVER["HTTP_USER_AGENT"]);
}

$isBot = isBot();
$_SESSION['MobilityDetected'] = isMobileDevice();

// Protect against GLOBALS tricks
if (isset($_POST['GLOBALS']) || isset($_FILES['GLOBALS']) || isset($_GET['GLOBALS']) || isset($_COOKIE['GLOBALS']))
{
	die("Hacking attempt");
}

if (isset($_GET['sid']) && !preg_match('/^[A-Za-z0-9]*$/', $_GET['sid'])) 
{
	$_GET['sid'] = '';
}
if (isset($_POST['sid']) && !preg_match('/^[A-Za-z0-9]*$/', $_POST['sid'])) 
{
	$_POST['sid'] = '';
}

$PHP_SELF = ($_SERVER['PHP_SELF']) ? $_SERVER['PHP_SELF'] : $_ENV['PHP_SELF'];

//
// Define some basic configuration arrays this also prevents
// malicious rewriting of language and otherarray values via
// URI params
//
$board_config = array();
$shoutbox_config = array();
$portal_config = array();
$attach_config = array();
$userdata = array();
$theme = array();
$images = array();
$lang = array();
$nav_links = array();
$gen_simple_header = FALSE;

include($phpbb_root_path . 'config.'.$phpEx);

if( !defined("PHPBB_INSTALLED") )
{
	header('Location: ' . $phpbb_root_path . 'install.' . $phpEx);
	exit;
}

include($phpbb_root_path . 'includes/constants.'.$phpEx);
include($phpbb_root_path . 'includes/template.'.$phpEx);
include($phpbb_root_path . 'includes/sessions.'.$phpEx);
include($phpbb_root_path . 'includes/auth.'.$phpEx);
include($phpbb_root_path . 'includes/functions.'.$phpEx);
include($phpbb_root_path . 'includes/db.'.$phpEx);

//
// addslashes to vars if magic_quotes_gpc is off
// this is a security precaution to prevent someone
// trying to break out of a SQL statement.
//
$mquotes = (!get_magic_quotes_gpc()) ? false : true;
var_adds($_GET, false);
var_adds($_POST, true, true);
var_adds($_COOKIE, false);

// We do not need this any longer, unset for safety purposes
unset($dbpasswd);

$subdirectory = '';

// Mozilla navigation bar
// Default items that should be valid on all pages.
// Defined here and not in page_header.php so they can be redefined in the code
$nav_links['top'] = array ( 
	'url' => append_sid($phpbb_root_dir."index.".$phpEx),
    'title' => sprintf(isset($lang['Forum_Index']) ? $lang['Forum_Index'] : '%s', isset($board_config['sitename']) ? $board_config['sitename'] : 'Home')
);
$nav_links['search'] = array ( 
	'url' => append_sid($phpbb_root_dir."search.".$phpEx),
    'title' => isset($lang['Search']) ? $lang['Search'] : 'Search'
);
$nav_links['help'] = array ( 
	'url' => append_sid($phpbb_root_dir."faq.".$phpEx),
    'title' => isset($lang['FAQ']) ? $lang['FAQ'] : 'FAQ'
);
$nav_links['author'] = array ( 
	'url' => append_sid($phpbb_root_dir."memberlist.".$phpEx),
    'title' => isset($lang['Memberlist']) ? $lang['Memberlist'] : 'Memberlist'
);

//
// Obtain and encode users IP
//
// I'm removing HTTP_X_FORWARDED_FOR ... this may well cause other problems such as
// private range IP's appearing instead of the guilty routable IP, tough, don't
// even bother complaining ... go scream and shout at the idiots out there who feel
// "clever" is doing harm rather than good ... karma is a great thing ... :)
//
$client_ip = ( !empty($_SERVER['REMOTE_ADDR']) ) ? $_SERVER['REMOTE_ADDR'] : ( ( !empty($_ENV['REMOTE_ADDR']) ) ? $_ENV['REMOTE_ADDR'] : getenv('REMOTE_ADDR') );
$user_ip = encode_ip($client_ip);

//
// Setup forum wide options, if this fails
// then we output a CRITICAL_ERROR since
// basic forum information is not available
//
$sql_work = true;
$board_config = sql_cache('check', 'board_config');
if (empty($board_config))
{
    $sql = "SELECT *
		FROM " . CONFIG_TABLE;
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(CRITICAL_ERROR, 'Nie można pobrać danych z tabeli konfiguracyjnej forum !<br /><br /><b>Prawdopodobnie forum nie jest zainstalowane do bazy danych, lub tabele w bazie danych maja inny prefix (standardowo phpbb_ ).<br />Sprawdz czy tabele w bazie danych maja prefix podany w pliku config.php<br /><br /><br />Jeżeli chcesz wgrać kopię bazy danych i wyslałes ja do katalogu forum użyj <a href="' . $phpbb_root_path . 'dbloader/dbloader.'.$phpEx . '">DumpLoader\'a</a></b><br />');
	}
	while ( $row = $db->sql_fetchrow($result) )
	{
		$board_config[$row['config_name']] = $row['config_value'];
	}

	$sql_work = sql_cache('write', 'board_config', $board_config);
}

if ( $board_config['protection_get'] && isset($_GET) )
{
	foreach($_GET as $key => $val)
	{
		if ( strlen($val) > 18 && !preg_match('#^[a-z0-9_ /+]*={0,2}$#i', $val) && !in_array($key, array('rdns', 'highlight', 'redirect', 'tag', 'search_author'))  )
		{
			die('Hacking attempt');
		}
	}
}

// gzip compression
$mod_deflate_check = true;
if ($board_config['gzip_compress'] && @extension_loaded('zlib') && !headers_sent() && ob_get_length() <= 1 && ob_get_length() == 0)
{
    if(@strtolower(@ini_get('zlib.output_compression')) != 'on' && @strtolower(@ini_get('output_handler')) != 'ob_gzhandler' && (int)@ini_get('zlib.output_compression') != 1)
    {
        if($mod_deflate_check) {
            $apache_modules = (function_exists('apache_get_modules')) ? apache_get_modules() : false;
            if(($apache_modules && !in_array('mod_deflate', $apache_modules)) || !$apache_modules) {
                ob_start("ob_gzhandler");
            }
        } else {
            ob_start("ob_gzhandler");
        }
    }
}

header('Content-type: text/html; charset=utf-8');

$board_config['topics_per_page'] = ($board_config['topics_per_page'] < 1) ? '25' : $board_config['topics_per_page'];
$board_config['posts_per_page'] = ($board_config['posts_per_page'] < 1) ? '25' : $board_config['posts_per_page'];
$board_config['hot_threshold'] = ($board_config['hot_threshold'] < 1) ? '25' : $board_config['hot_threshold'];
$board_config['session_length'] = ($board_config['session_length'] < 5) ? '3600' : $board_config['session_length'];

// if system administrator does not set correct timezone information, fix it to avoid warnings
if (ini_get('date.timezone')=='')
{
    // List of supported timezones could be found at http://www.php.net/manual/en/timezones.php
    //
    if ($board_config['default_lang'] == 'polish')
    {
        @ini_set('date.timezone', 'Europe/Warsaw');
    }
    elseif ($board_config['default_lang'] == 'english')
    {
        @ini_set('date.timezone', 'Europe/London'); // timezone +0:00
    }
    else
    {
        @ini_set('date.timezone', 'UTC');
    }
}

if ( empty($board_config['server_name']) )
{
	if ( !empty($_SERVER['SERVER_NAME']) || !empty($_ENV['SERVER_NAME']) )
	{
		$hostname = ( !empty($_SERVER['SERVER_NAME']) ) ? $_SERVER['SERVER_NAME'] : $_ENV['SERVER_NAME'];
	}
	else if ( !empty($_SERVER['HTTP_HOST']) || !empty($_ENV['HTTP_HOST']) )
	{
		$hostname = ( !empty($_SERVER['HTTP_HOST']) ) ? $_SERVER['HTTP_HOST'] : $_ENV['HTTP_HOST'];
	}
	else
	{
		$hostname = '';
	}
	$board_config['server_name'] = $hostname;
}

$board_config['cookie_domain'] = $board_config['server_name'] = trim($board_config['server_name']);

if ( defined('SHOUTBOX') || defined('IN_ADMIN') )
{
	$shoutbox_config = sql_cache('check', 'shoutbox_config');
	if (empty($shoutbox_config))
	{
		$sql = "SELECT *
			FROM " . SHOUTBOX_CONFIG_TABLE;
		if ( !($result = $db->sql_query($sql)) )
		{
			message_die(CRITICAL_ERROR, 'Could not query shoutbox config information', '', __LINE__, __FILE__, $sql);
		}

		while ( $row = $db->sql_fetchrow($result) )
		{
			$shoutbox_config[$row['config_name']] = $row['config_value'];
		}
		sql_cache('write', 'shoutbox_config', $shoutbox_config);
	}
}

if ( !(defined('SHOUTBOX')) )
{
	$portal_config = sql_cache('check', 'portal_config');
	if (empty($portal_config))
	{
		$sql = "SELECT * FROM " . PORTAL_CONFIG_TABLE;
		if ( !($result = $db->sql_query($sql)) )
		{
			message_die(GENERAL_ERROR, 'Could not query portal config information', '', __LINE__, __FILE__, $sql);
		}

		while ( $row = $db->sql_fetchrow($result) )
		{
			$portal_config[$row['config_name']] = $row['config_value'];
		}
		sql_cache('write', 'portal_config', $portal_config);
	}
}

$portal_config_portal_on = isset($portal_config['portal_on']) ? $portal_config['portal_on'] : null;
$portal_config_link_logo = isset($portal_config['link_logo']) ? $portal_config['link_logo'] : null;
$portal_config_witch_news_forum = isset($portal_config['witch_news_forum']) ? $portal_config['witch_news_forum'] : null;

if ( (!(defined('PORTAL')) && !(defined('IN_ADMIN'))) || isset($portal_config['portal_on']) && $portal_config['portal_on'] != 1 )
{
	unset($portal_config);
}

$unique_cookie_name = 'bb' . substr(md5($board_config['cookie_name'] . $board_config['script_path']), 5, 8);

if ( defined('ATTACH') || defined('IN_ADMIN') )
{
	$attach_config = sql_cache('check', 'attach_config');
	if (empty($attach_config))
	{
		$sql = "SELECT * FROM " . ATTACH_CONFIG_TABLE;
		if ( !($result = $db->sql_query($sql)) )
		{
			message_die(CRITICAL_ERROR, 'Could not query attachments config information', '', __LINE__, __FILE__, $sql);
		}
		while ( $row = $db->sql_fetchrow($result) )
		{
			$attach_config[$row['config_name']] = $row['config_value'];
		}
		sql_cache('write', 'attach_config', $attach_config);
	}
	if ( !$attach_config['disable_mod'] || defined('IN_ADMIN'))
	{
		define('ATTACHMENTS_ON', true);
		include($phpbb_root_path . 'attach_mod/attachment_mod.'.$phpEx);
	}
}

if ( isset($board_config['db_backup_enable']) && $board_config['db_backup_enable']>0 && isset($board_config['db_backup_time']) && $board_config['db_backup_time'] < (CR_TIME - (24 * 3600)) && !defined('IN_ADMIN') )
{
	include($phpbb_root_path . 'includes/functions_admin.'.$phpEx);
	db_backup();
}

if (!empty($_COOKIE[$unique_cookie_name . '_b']))
{
	message_die(GENERAL_MESSAGE, 'You_been_banned');
}

if(isset($result)) $db->sql_freeresult($result);

?>