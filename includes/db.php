<?php
/***************************************************************************
 *                                 db.php
 *                            -------------------
 *   begin                : Saturday, Feb 13, 2001
 *   copyright            : (C) 2001 The phpBB Group
 *   email                : support@phpbb.com
 *
 *   $Id: db.php,v 1.10.2.3 2005/10/30 15:17:14 acydburn Exp $
 *
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

if ( !defined('IN_PHPBB') )
{
	die("Hacking attempt");
}

include($phpbb_root_path . 'db/pdo.'.$phpEx);

// Make the database connection.
$db = new sql_db($config['database']);
$dbname = $db->getDBName();
if(!$db->isConnected())
{
	$err = $db->sql_error(true);
	$debug_txt = (DEBUG) ? '<br /><b>' . $err['message']. ' </b>' : '';
	message_die(CRITICAL_ERROR, 'Could not connect to the database.' . $debug_txt);
}