<?php

define('IN_PHPBB', true);
define('ATTACH', true);
$phpbb_root_path = './';
include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.'.$phpEx);

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_INDEX);
init_userprefs($userdata); 



// Pobieranie zawartości

if (!$userdata['session_logged_in'] )
{
		echo '<b><font color="red">UWAGA!</font></b> Ze względów bezpieczeństwa zostałeś wylogowany.<br>Zostaniesz przekierowany do strony logowania.<br>Treść została zachowana, jednak na wszelki wypadek skopiuj treść postu <b>(CTRL+A,CTRL+C)</b><br>';
}

else
{
	$gdzie = 'tmp/'.$userdata['user_id'].'bakp';
	
	if(empty($_POST))
	{
		if(file_exists($gdzie))
		{
			$plik=file_get_contents($gdzie); 
			message_die(GENERAL_MESSAGE, $plik, 'Oto Twoja wiadomość. Możesz ją teraz skopiować i wysłać ponownie:');
			//include $gdzie;
			}
		else message_die(GENERAL_MESSAGE, 'W ostatnim czasie niczego nie pisałeś :)', 'Brak postów do przywrócenia');
	}
	
	else
	{
		$subject = htmlspecialchars($_POST['subject']);
		$message = str_replace("\n", "<br>", htmlspecialchars($_POST['message']));

		$kod = "<!doctype HTML><html><head><title>".date("H:i")."</title><meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"></head><body>
		Temat: ".$subject."<br> Treść: ".$message."</body></html>"; // Wygląd pliku do zapisu


		if(file_put_contents($gdzie, $kod)) // zapis kodu do pliku
		{
			echo 'Kopia z: ', date("H:i"), ' <a href="restore.php" target=_blank>(zobacz)</a>';
		}
	
		else echo "Wystąpił problem.";
	}
}

?>